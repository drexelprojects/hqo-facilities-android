/**
 * ParkingPassTicket.java
 * Represents an application for a parking pass.
 */
package com.example.facilities;

import android.graphics.Bitmap;

import java.util.HashMap;

public class ParkingPassTicket extends Ticket {
    private String badgeNumber;
    private String firstName;
    private String lastName;
    private String email;
    private boolean hasMultipleVehicles;
    private boolean isHandicapRegistered;
    private HashMap<Integer, Bitmap> images = new HashMap<>();
    private String prefContactMethod;
    private String altContactInfo;
    private boolean isContractor;

    private RegisteredVehicle vehicle;

    /**
     * Constructor for {@code ParkingPassTicket}. Sets details for the person intended to receive
     * the parking pass as well as additional details regarding their vehicle and parking accomodations.
     * @param badgeNumber Badge number
     * @param firstName First name
     * @param lastName Last name
     * @param email Email
     * @param vehicle Vehicle
     * @param hasMultipleVehicles Whether or not the pass will be used for multiple vehicles.
     * @param isHandicapRegistered Whether or not the person has handicap parking permissions.
     * @param prefContactMethod Preferred contact method of the user (email, message, call).
     * @param altContactInfo Alternate contact info, based on preferred contact method.
     * @param isContractor Whether or not the user is a contractor.
     */
    public ParkingPassTicket(String badgeNumber, String firstName, String lastName, String email, RegisteredVehicle vehicle, boolean hasMultipleVehicles, boolean isHandicapRegistered, String prefContactMethod, String altContactInfo, boolean isContractor) {
        super("TYPE_PARKING_PASS");

        this.badgeNumber = badgeNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.vehicle = vehicle;
        this.email = email;
        this.hasMultipleVehicles = hasMultipleVehicles;
        this.isHandicapRegistered = isHandicapRegistered;
        this.prefContactMethod = prefContactMethod;
        this.altContactInfo = altContactInfo;
        this.isContractor = isContractor;
    }

    /**
     * @return Badge number
     */
    public String getBadgeNumber() {
        return this.badgeNumber;
    }

    /**
     * @return First name of recipient.
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * @return Last name of recipient.
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * @return Email of recipient.
     */
    public String getEmail() { return this.email; }

    /**
     * @return {@code LicensePlate} of registered vehicle.
     */
    public String getLicensePlate() { return this.vehicle.getLicensePlate().getLicensePlateNumber(); }

    /**
     * @return State of the vehicle registration.
     */
    public String getState() {
        return this.vehicle.getLicensePlate().getState();
    }

    /**
     * @return Color of the vehicle.
     */
    public String getColor() {
        return this.vehicle.getColor();
    }

    /**
     * @return Make of the vehicle.
     */
    public String getMake() {
        return this.vehicle.getMake();
    }

    /**
     * @return Model of the vehicle.
     */
    public String getModel() {
        return this.vehicle.getModel();
    }

    /**
     * @return Year that recipient's vehicle was manufactured.
     */
    public int getYear() {
        return this.vehicle.getYear();
    }

    /**
     * @return True if requires electric charging, false otherwise.
     */
    public boolean isElectric() {
        return this.vehicle.isElectric();
    }

    /**
     * @return True if it will be used for multiple vehicles, false otherwise.
     */
    public boolean hasMultipleVehicles() {
        return this.hasMultipleVehicles;
    }

    /**
     * @return True if handicap registered, false otherwise.
     */
    public boolean isHandicapRegistered() {
        return this.isHandicapRegistered;
    }

    /**
     * @return The handicap image either in the form of the license plate icon or a tag.
     */
    public HashMap<Integer, Bitmap> getImages() {
        return this.images;
    }

    /**
     * Set the handicap image for a parking pass ticket.
     * @param images Images to be assigned to {@code images}.
     */
    public void setImages(HashMap<Integer, Bitmap> images) {
        this.images = images;
    }

    /**
     * @return Preferred contact method (email, message, calling).
     */
    public String getPrefContactMethod() { return this.prefContactMethod; }

    /**
     * @return Alternate contact information.
     */
    public String getAltContactInfo() { return this.altContactInfo; }

    /**
     * @return True if contractor, false otherwise.
     */
    public boolean isContractor() {
        return this.isContractor;
    }
}