/**
 * ParkingPassTicketForm.java
 * Form to submit request for a parking pass.
 */
package com.example.facilities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.ImageButton;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import java.io.File;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;

public class ParkingPassTicketForm extends TicketForm implements  AsyncResponse {
    private static final int SCAN_LICENSE_PLATE = 4;
    private static final String TAG = "PARKING_PASS_FORM";

    // Form inputs/input containers
    private EditText editTextBadgeNumber, editTextLicensePlate, editTextContactInfo;
    private Spinner spinnerState;
    private TableLayout layoutColors;
    private SwitchCompat switchMultiVehicle, switchEV, switchHandicap;
    private TextView textViewAddImageInstructions, textViewColors;
    private Button btnAddImage;
    private ImageView imageView1;
    private HashMap<Integer, Bitmap> images = new HashMap<>();
    private LinearLayout layoutContactMethod;
    private ConstraintLayout layoutLoading;
    private String licensePlate;
    private ConstraintLayout helpOverlayLayout;
    private Uri photoUri;

    public ParkingPassTicketForm(){}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_pass_form);

        this.initWidgets();
        this.checkPermission();
    }

    /**
     * Navigate to the {@code TicketList}.
     * @param view The calling {@code view}.
     */
    public void navTicketList(View view) {
        Intent intent = new Intent(this, TicketList.class);
        startActivity(intent);
    }

    /**
     * Handle color selection. If not already activated, the view is activated
     * and then all other views within the ViewGroup are deactivated.
     * @param view The color View to be selected.
     */
    public void selectColor(View view) {
        this.hideHelpOverlay();
        this.textViewColors.setError(null);

        if (!view.isActivated()) {
            // Deactivate other options
            ViewGroup table = (ViewGroup) view.getParent().getParent();
            for (int i=0; i<table.getChildCount(); i++) {
                ViewGroup options = (ViewGroup) table.getChildAt(i);
                deactivateViewGroup(options);
            }

            // Activate intended color
            view.setActivated(true);
        }
    }

    /**
     * Handle contact method preference selection. If not already activated,
     * the view is activated and then all other views within the ViewGroup are deactivated.
     * @param view The contact method View to be selected.
     */
    public void selectContactMethod(View view) {
        this.hideHelpOverlay();

        if (!view.isActivated()) {
            // Deactivate other options
            ViewGroup options = (ViewGroup) view.getParent();
            deactivateViewGroup(options);

            // Activate intended option
            view.setActivated(true);
        }
    }

    /**
     * Deselects all items in a ViewGroup.
     * @param viewGroup ViewGroup containing all views that will be deactivated.
     */
    private void deactivateViewGroup(ViewGroup viewGroup) {
        for (int i=0; i<viewGroup.getChildCount(); i++) {
            View currentView = viewGroup.getChildAt(i);
            currentView.setActivated(false);
        }
    }

    /**
     * Gets currently activated item in ViewGroup.
     * @param viewGroup ViewGroup containing all views that can be in an
     *                  activated/deactivated state.
     * @return String value of the view that is currently selected.
     */
    private String getActivatedViewValue(ViewGroup viewGroup) {
        for (int j=0; j<viewGroup.getChildCount(); j++) {
            View option = (View) viewGroup.getChildAt(j);
            if (option.isActivated()) {
                return option.getContentDescription().toString();
            }
        }

        return null;
    }

    /**
     * Check if permission has already been granted to access camera.
     * If false, disable Add Image button and request permission from user to access camera.
     */
    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED) {
            btnAddImage.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0);
        }
    }

    /**
     * Grant permission if user allows it. Enable Add Image button.
     * @param requestCode The request code passed in requestPermissions
     * @param permissions The array of manifest permissions
     * @param grantResults The grant results for the corresponding permissions which is either PERMISSION_GRANTED or PERMISSION_DENIED.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                btnAddImage.setEnabled(true);
            }
        }
    }

    /**
     * Opens an AlertDialog to ask user whether they want to take a photo or choose from the phone's library.
     * Starts new intents that opens camera or photo library.
     * User can click "Cancel" to close dialog.
     */
    public void addImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ParkingPassTicketForm.this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    // User chooses to take a live photo
                    dispatchTakePhotoIntent();
                } else if (items[item].equals("Choose from Library")) {
                    // User chooses to select an item from their device library.
                    Intent choosePhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    if (choosePhoto.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(choosePhoto, REQUEST_IMAGE_GALLERY);
                    }
                } else if (items[item].equals("Cancel")) {
                    // User chooses to cancel adding an image.
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Save file for image and start camera intent
     */
    private void dispatchTakePhotoIntent() {
        // Take live photo
        Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePhoto.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                Log.d(TAG, e.toString());
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoUri = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);

            }
            takePhoto.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(takePhoto, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * Start the ScanLicensePlateActivity
     * @param view The calling view
     */
    public void scanLicensePlate(View view) {
        hideHelpOverlay();

        Intent intent = new Intent(this, ScanLicensePlateActivity.class);
        startActivityForResult(intent, SCAN_LICENSE_PLATE);
    }

    /**
     * Called when activity launched by startActivityForResult() exits.
     * @param requestCode The integer request code originally supplied to startActivityForResult(), allowing you to identify who this result came from.
     * @param resultCode The integer result code returned by the child activity through its setResult().
     * @param data An Intent, which can return result data to the caller.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                // Image from camera
                Bitmap imageBitmap = null;
                try {
                    imageBitmap = handleSamplingAndRotationBitmap(this, photoUri);
                } catch (IOException e) {
                    Log.d(TAG, e.toString());
                }
                addBitmapToImageView(imageBitmap);
            } else if (requestCode == REQUEST_IMAGE_GALLERY) {
                // Image from gallery
                if (data != null) {
                    Uri contentURI = data.getData();
                    new LoadImageDataTask(contentURI).execute();
                }
            } else if (requestCode == SCAN_LICENSE_PLATE) {
                // Scan the license plate
                if (data != null && data.getExtras() != null) {
                    licensePlate = data.getExtras().getString("licensePlate");
                    editTextLicensePlate.setText(licensePlate);
                }
            }
        }
    }

    /**
     * Check the image view to see if a bitmap image has already been set.
     * Set imageBitmap to image view that is not visible.
     * @param imageBitmap The bitmap image passed from onActivityResults
     */
    protected void addBitmapToImageView(Bitmap imageBitmap) {
        if (imageView1.getVisibility() == View.GONE) {
            imageView1.setImageBitmap(imageBitmap);
            imageView1.setVisibility(View.VISIBLE);
            images.put(1, imageBitmap);
        }
    }

    /**
     * Checks for validity of form. Proceeds to {@code success()} if valid. Returns otherwise.
     * @param view The calling {@code view}.
     */
    public void submit(View view) {
        view.setEnabled(false);
        if (isValid()) {
            success();
        } else {
            view.setEnabled(true);
        }

        return;
    }

    /**
     * Checks if all fields of the parking pass form have been completed.
     * Includes only fields that do not have a default state which defaults to a valid option.
     * @return Whether or not the form is valid.
     */
    private boolean isValid() {
        boolean isValid = true;

        // Badge number
        String badgeNumber = editTextBadgeNumber.getText().toString();
        String badgeRegex = "^[0-9]{9}$";
        Pattern badgePattern = Pattern.compile(badgeRegex);
        Matcher badgeMatcher = badgePattern.matcher(badgeNumber);
        if (TextUtils.isEmpty(badgeNumber)) {   // Check if empty
            editTextBadgeNumber.setError("Please enter a badge number.");
            isValid = false;
        } else if (!badgeMatcher.find()) {      // Check if valid badge number
            editTextBadgeNumber.setError("The badge number should consist of nine digits.");
            isValid = false;
        }

        // State selection; false if still on default value of "State*"
        if (spinnerState.getSelectedItemPosition() == 0) {
            ((TextView) spinnerState.getSelectedView()).setError("Please select a state.");
            isValid = false;
        }

        // License plate
        // Check if empty
        String licensePlateNumber = editTextLicensePlate.getText().toString().toLowerCase();
        boolean stateValidation = plateValidbyState(licensePlateNumber);

        if (TextUtils.isEmpty(licensePlateNumber)) {
            editTextLicensePlate.setError("Please enter a license plate number.");
            isValid = false;
        } else if (!stateValidation) { // Check if valid value license plate number
            editTextLicensePlate.setError("The license plate number does not match the state's specified format." +
                    "If it is a vanity plate, please select CUSTOM in the drop down.");
            isValid = false;
        }

        // Color
        String color = "";
        for (int i=0; i<layoutColors.getChildCount(); i++) {
            ViewGroup layoutColorRow = (ViewGroup) layoutColors.getChildAt(i);
            color = this.getActivatedViewValue(layoutColorRow);

            if (color != null && !color.isEmpty()) {
                break;
            }
        }
        if (color == null || color.isEmpty()) {
            this.textViewColors.setError("Please select at least one color.");
            isValid = false;
        }

        // Contact information
        String emailRegex = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        Pattern emailPattern = Pattern.compile(emailRegex);
        Matcher emailMatcher = emailPattern.matcher(editTextContactInfo.getText().toString());

        String prefContactMethod = getActivatedViewValue(layoutContactMethod);

        String phoneRegex = "^[\\d]{10}$";
        Pattern phonePattern = Pattern.compile(phoneRegex);
        Matcher phoneMatcher = phonePattern.matcher(editTextContactInfo.getText().toString());

        if (TextUtils.isEmpty(editTextContactInfo.getText().toString())) {
            editTextContactInfo.setError("Please enter contact information.");
            isValid = false;
        } else if(prefContactMethod.equals("Email") && !emailMatcher.matches()){
            editTextContactInfo.setError("Please enter a valid email.");
            isValid = false;
        } else if((prefContactMethod.equals("Call") || prefContactMethod.equals("Message")) && !phoneMatcher.matches()){
            editTextContactInfo.setError("Please enter a valid phone number.");
            isValid = false;
        }

        return isValid;
    }

    /**
     * Check if license plate entered matches the serial format
     * of the state selected
     * @param licensePlateNumber
     * @return boolean if regex is matched, or if CUSTOM, returns true
     */
    private boolean plateValidbyState(String licensePlateNumber) {
        licensePlateNumber = licensePlateNumber.replaceAll("-", "").trim().toLowerCase();

        String state = spinnerState.getSelectedItem().toString();
        if (state.equals("AL")){
            //0AB1234
            //00AB123
            String licenseRegex1 = "^[\\d]{2}[a-z]{2}[\\d]{3}$";
            String licenseRegex2 = "^[\\d]{1}[a-z]{2}[\\d]{4}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Pattern licensePattern2 = Pattern.compile(licenseRegex2);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            Matcher licenseMather2 = licensePattern2.matcher(licensePlateNumber);
            return licenseMatcher1.matches() || licenseMather2.matches();
        }
        else if(state.equals("AK")){
            //ABC123
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }
        else if(state.equals("AZ")){
            //ABC1234
            String licenseRegex = "^[a-z]{4}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }
        else if(state.equals("AR")){
            //123ABC
            String licenseRegex = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }
        else if(state.equals("CA")){
            //1ABC234
            String licenseRegex = "^[\\d][a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }
        else if(state.equals("CO")){
            //123ABC, ABC123
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            String licenseRegex1 = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }
        else if(state.equals("CT")){
            //AB12345; 1ABCD2;123ABC
            String licenseRegex = "^[a-z]{2}[\\d]{5}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d][a-z]{4}[\\d]$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);

            String licenseRegex2 = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern2 = Pattern.compile(licenseRegex2);
            Matcher licenseMatcher2 = licensePattern2.matcher(licensePlateNumber);

            return licenseMatcher.matches() || licenseMatcher1.matches() || licenseMatcher2.matches();
        }
        else if(state.equals("DE")){
            //123456
            String licenseRegex = "^[\\d]{6}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }
        else if(state.equals("FL")){
            //ABCD12; 123ABC; A123BC; 1234AB
            String licenseRegex = "^[a-z]{4}[\\d]{2}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);

            String licenseRegex2 = "^[a-z][\\d]{3}[a-z]{2}$";
            Pattern licensePattern2 = Pattern.compile(licenseRegex2);
            Matcher licenseMatcher2 = licensePattern2.matcher(licensePlateNumber);

            String licenseRegex3 = "^[\\d]{4}[a-z]{2}$";
            Pattern licensePattern3 = Pattern.compile(licenseRegex3);
            Matcher licenseMatcher3 = licensePattern3.matcher(licensePlateNumber);

            return licenseMatcher.matches() || licenseMatcher1.matches() || licenseMatcher2.matches() || licenseMatcher3.matches();
        }
        else if(state.equals("GA")){
            //ABC1234
            String licenseRegex = "^[a-z]{3}[\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }
        else if(state.equals("HI")){
            String licenseRegex = "^[a-hjnpr-yhzklm]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }
        else if(state.equals("ID")){
            //A123456; 0A12345; 0AB1234; 0ABC123; 0A1B234; 0A1234B; 00A1234
            String licenseRegex = "^[a-z][\\d]{6}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d][a-z][\\d]{5}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);

            String licenseRegex2 = "^[\\d][a-z]{2}[\\d]{4}$";
            Pattern licensePattern2 = Pattern.compile(licenseRegex2);
            Matcher licenseMatcher2 = licensePattern2.matcher(licensePlateNumber);

            String licenseRegex3 = "^[\\d][a-z]{3}[\\d]{3}$";
            Pattern licensePattern3 = Pattern.compile(licenseRegex3);
            Matcher licenseMatcher3 = licensePattern3.matcher(licensePlateNumber);

            String licenseRegex4 = "^[\\d][a-z][\\d][a-z][\\d]{3}$";
            Pattern licensePattern4 = Pattern.compile(licenseRegex4);
            Matcher licenseMatcher4 = licensePattern4.matcher(licensePlateNumber);

            String licenseRegex5 = "^[\\d][a-z][\\d]{4}[a-z]$";
            Pattern licensePattern5 = Pattern.compile(licenseRegex5);
            Matcher licenseMatcher5 = licensePattern5.matcher(licensePlateNumber);

            String licenseRegex6 = "^[\\d]{2}[a-z][\\d]{4}$";
            Pattern licensePattern6 = Pattern.compile(licenseRegex6);
            Matcher licenseMatcher6 = licensePattern6.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches() || licenseMatcher2.matches() || licenseMatcher3.matches() ||
                    licenseMatcher4.matches() || licenseMatcher5.matches() || licenseMatcher6.matches();
        }

        else if(state.equals("IL")){
            //AB12345
            String licenseRegex = "^[a-z]{2}[\\d]{5}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("IN")){
            //123A; 123AB; 123ABC
            String licenseRegex = "^[\\d]{3}[a-z]{1,3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("IA")){
            //ABC123
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("KS")){
            //123ABC
            String licenseRegex = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("KY")){
            //123ABC
            String licenseRegex = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("LA")){
            //abc123; 123abc
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("ME")){
            //1234AB
            String licenseRegex = "^[\\d]{4}[a-z]{2}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("MD")){
            //1ab2345
            String licenseRegex = "^[\\d][a-z]{2}[\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("MA")){
            //12a345; 1abc23; 1ab234; 123ab4; 12ab34; 1234ab; 123abc
            String licenseRegex = "^[\\d]{2}[a-z][\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d][a-z]{3}[\\d]{2}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);

            String licenseRegex2 = "^[\\d][a-z]{2}[\\d]{3}$";
            Pattern licensePattern2 = Pattern.compile(licenseRegex2);
            Matcher licenseMatcher2 = licensePattern2.matcher(licensePlateNumber);

            String licenseRegex3 = "^[\\d]{3}[a-z]{2}[\\d]$";
            Pattern licensePattern3 = Pattern.compile(licenseRegex3);
            Matcher licenseMatcher3 = licensePattern3.matcher(licensePlateNumber);

            String licenseRegex4 = "^[\\d]{2}[a-z]{2}[\\d]{2}$";
            Pattern licensePattern4 = Pattern.compile(licenseRegex4);
            Matcher licenseMatcher4 = licensePattern4.matcher(licensePlateNumber);

            String licenseRegex5 = "^[\\d]{4}[a-z]{2}$";
            Pattern licensePattern5 = Pattern.compile(licenseRegex5);
            Matcher licenseMatcher5 = licensePattern5.matcher(licensePlateNumber);

            String licenseRegex6 = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern6 = Pattern.compile(licenseRegex6);
            Matcher licenseMatcher6 = licensePattern6.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches() || licenseMatcher2.matches() || licenseMatcher3.matches() ||
                    licenseMatcher4.matches() || licenseMatcher5.matches() || licenseMatcher6.matches();
        }

        else if(state.equals("MI")){
            //ABC1234; 1ABC23; ABC123
            String licenseRegex = "^[a-z]{3}[\\d]{3,4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d][a-z]{3}[\\d]{2}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);

            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("MN")){
            //123abc
            String licenseRegex = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("MS")){
            //abc123
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("MO")){
            //AA1B2C
            String licenseRegex = "^[a-z]{2}[\\d][a-z][\\d][a-z]$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("MT")){
            //012345A; 001234A; ABC123
            String licenseRegex = "^[\\d]{6}[a-z]$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);

            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("NE")){
            //ABC123; 0a1234; 0ab123; 00a123; 00ab12
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d][a-z][\\d]{4}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);

            String licenseRegex2 = "^[\\d][a-z]{2}[\\d]{3}$";
            Pattern licensePattern2 = Pattern.compile(licenseRegex2);
            Matcher licenseMatcher2 = licensePattern2.matcher(licensePlateNumber);

            String licenseRegex3 = "^[\\d]{2}[a-z][\\d]{3}$";
            Pattern licensePattern3 = Pattern.compile(licenseRegex3);
            Matcher licenseMatcher3 = licensePattern3.matcher(licensePlateNumber);

            String licenseRegex4 = "^[\\d]{2}[a-z]{2}[\\d]{2}$";
            Pattern licensePattern4 = Pattern.compile(licenseRegex4);
            Matcher licenseMatcher4 = licensePattern4.matcher(licensePlateNumber);

            return licenseMatcher.matches() || licenseMatcher1.matches() || licenseMatcher2.matches() || licenseMatcher3.matches() ||
                    licenseMatcher4.matches();
        }

        else if(state.equals("NV")){
            //123a45
            String licenseRegex = "^[\\d]{3}[a-z][\\d]{2}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("NH")){
            //1234567; 123456
            String licenseRegex = "^[\\d]{6,7}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("NJ")){
            //d12abc; abc12d
            String licenseRegex = "^[a-z][\\d]{2}[a-z]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[a-z]{3}[\\d]{2}[a-z]$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("NM")){
            //123ABC; abc123
            String licenseRegex = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("NY")){
            //abc1234
            String licenseRegex = "^[a-z]{3}[\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("NC")){
            //abc123; abc1234
            String licenseRegex = "^[a-z]{3}[\\d]{3,4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("ND")){
            //123abc
            String licenseRegex = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("OH")){
            //abc1234
            String licenseRegex = "^[a-z]{3}[\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("OK")){
            //abc123
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("OR")){
            //123abc; abc123
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("PA")){
            //abc1234
            String licenseRegex = "^[a-z]{3}[\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("RI")){
            //123456; 12345; AB123
            String licenseRegex = "^[a-z]{2}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d]{5,6}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("SC")){
            //abc123; 1234ab
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d]{4}[a-z]{2}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("SD")){
            //0A1234; 0AB123; 00A123
            String licenseRegex = "^[\\d][a-z][\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d][a-z]{2}[\\d]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);

            String licenseRegex2 = "^[\\d]{2}[a-z][\\d]{3}$";
            Pattern licensePattern2 = Pattern.compile(licenseRegex2);
            Matcher licenseMatcher2 = licensePattern2.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches() || licenseMatcher2.matches();
        }

        else if(state.equals("TN")){
            //a1234b; 1a23b4
            String licenseRegex = "^[a-z][\\d]{4}[a-z]$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d][a-z][\\d]{2}[a-z][\\d]$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("TX")){
            //abc1234
            String licenseRegex = "^[a-z]{3}[\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("UT")){
            //a123bc; 1a2bc
            String licenseRegex = "^[a-z][\\d]{3}[a-z]{2}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d][a-z][\\d][a-z]{2}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("VT")){
            //abc123; 12ab3; 1ab23; 123a4; 1a234
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d]{2}[a-z]{2}[\\d]$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);

            String licenseRegex2 = "^[\\d][a-z]{2}[\\d]{2}$";
            Pattern licensePattern2 = Pattern.compile(licenseRegex2);
            Matcher licenseMatcher2 = licensePattern2.matcher(licensePlateNumber);

            String licenseRegex3 = "^[\\d]{3}[a-z][\\d]$";
            Pattern licensePattern3 = Pattern.compile(licenseRegex3);
            Matcher licenseMatcher3 = licensePattern3.matcher(licensePlateNumber);

            String licenseRegex4 = "^[\\d][a-z][\\d]{3}$";
            Pattern licensePattern4 = Pattern.compile(licenseRegex4);
            Matcher licenseMatcher4 = licensePattern4.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches() || licenseMatcher2.matches() ||
                    licenseMatcher3.matches() || licenseMatcher4.matches();
        }

        else if(state.equals("VA")){
            //abc1234
            String licenseRegex = "^[a-z]{3}[\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);
            return licenseMatcher.matches();
        }

        else if(state.equals("WA")){
            //abc1234; 123abc
            String licenseRegex = "^[a-z]{3}[\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("WV")){
            //0ab123; abc123
            String licenseRegex = "^[a-z]{3}[\\d]{3}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d][a-z]{2}[\\d]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("WI")){
            //abc1234; 123abc
            String licenseRegex = "^[a-z]{3}[\\d]{4}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d]{3}[a-z]{3}$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("WY")){
            //012345; 01234A; 0012345
            String licenseRegex = "^[\\d]{6,7}$";
            Pattern licensePattern = Pattern.compile(licenseRegex);
            Matcher licenseMatcher = licensePattern.matcher(licensePlateNumber);

            String licenseRegex1 = "^[\\d]{5}[a-z]$";
            Pattern licensePattern1 = Pattern.compile(licenseRegex1);
            Matcher licenseMatcher1 = licensePattern1.matcher(licensePlateNumber);
            return licenseMatcher.matches() || licenseMatcher1.matches();
        }

        else if(state.equals("CUSTOM")){
            //vanity plate
            return true;
        }

        else{
            Log.d(TAG, "plateValidbyState: No state was passed!");
            return false;
        }

    }

    /**
     * Upon successful validation of form, submit the data to the database. Retrieves the inputted
     * values for all fields on the form. Starts the {@code AsyncTask}s for retrieving the year make model
     * and then database submission.
     */
    private void success() {
        this.layoutLoading.setVisibility(View.VISIBLE);

        String badgeNumber = editTextBadgeNumber.getText().toString();
        String userBadgeNumber = "";
        try {
            userBadgeNumber = FacilityApp.user.getString("badge_number");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        boolean isContractor = false;
        String firstName = "", lastName = "", email = "";
        if (badgeNumber.equals(userBadgeNumber)) {
            // If badge number is equal, isContractor stays false and set data equal to firstName and lastName
            try {
                firstName = FacilityApp.user.getString("first_name");
                lastName = FacilityApp.user.getString("last_name");
                email = FacilityApp.user.getString("email");
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
        } else {
            // If badge numbers are not equal, then retrieve data from user database (AW)
            isContractor = true;
            firstName = "Joe";
            lastName = "Carter";
            email = "joe.carter@amwater.com";
        }

        // Retrieve car details based on license plate and state
        String licensePlate = editTextLicensePlate.getText().toString();
        licensePlate = licensePlate.replaceAll("-", "").trim().toLowerCase();
        String state = spinnerState.getSelectedItem().toString();

        // Color
        String color = "";
        for (int i=0; i<layoutColors.getChildCount(); i++) {
            ViewGroup layoutColorRow = (ViewGroup) layoutColors.getChildAt(i);
            color = this.getActivatedViewValue(layoutColorRow);

            if (color != null && !color.isEmpty()) {
                break;
            }
        }

        boolean hasMultipleVehicles = switchMultiVehicle.isChecked();
        boolean isEV = switchEV.isChecked();
        boolean isHandicapRegistered = switchHandicap.isChecked();
        String prefContactMethod = getActivatedViewValue(layoutContactMethod);
        String altContactInfo = editTextContactInfo.getText().toString();

        try {
            // Adds all data to the JSONObject
            JSONObject data = new JSONObject();
            data.put("user_id", "vjt33");
            data.put("badge_no", badgeNumber);
            data.put("is_contractor", isContractor);
            data.put("first_name", firstName);
            data.put("last_name", lastName);
            data.put("email", email);
            data.put("license_plate", licensePlate);
            data.put("state", state);
            data.put("color", color);
            data.put("multiple", hasMultipleVehicles);
            data.put("is_ev", isEV);
            data.put("is_handicap", isHandicapRegistered);
            data.put("pref_contact", prefContactMethod);
            data.put("alt_contact", altContactInfo);

            // If provided, otherwise has blank string.
            Bitmap image = images.get(1);
            if (image != null) {
                ImageConverter converter = new ImageConverter();
                data.put("handicap_image", converter.convertBitmapToString(image));
            } else {
                data.put("handicap_image", "");
            }

            // Executes the task to retrieve the year, make, and model and then submit the data to the database.
            TaskYearMakeModel taskYearMakeModel = new TaskYearMakeModel(this, new TaskDatabaseSubmit(this, this, "https://modular-ground-255216.appspot.com/create_parking_pass?user_id=vjt33"));
            taskYearMakeModel.execute(data);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    /**
     * Checks the result to make sure that database submission went okay and then finishes the activity with
     * the corresponding result.
     * @param result Response from the async task.
     */
    @Override
    public void processFinish(JSONObject result) {
        try {
            if (result != null && result.getBoolean("success")) {
                this.layoutLoading.setVisibility(View.GONE);
                setResult(RESULT_OK);
            } else {
                setResult(RESULT_CANCELED);
            }
            ParkingPassTicketForm.this.finish();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    /**
     * Hides the help overlay if it is currently visible
     */
    private void hideHelpOverlay() {
        if (helpOverlayLayout.getVisibility() == View.VISIBLE) {
            helpOverlayLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Initialize widget(s) from the layout file and sets any required event listeners.
     */
    protected void initWidgets(){
        this.helpOverlayLayout = findViewById(R.id.helpOverlay);
        this.helpOverlayLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                view.setVisibility(View.GONE);
            }
        });
        if (!FacilityApp.tutorial.shown(getString(R.string.pref_parking_pass))) {
            this.helpOverlayLayout.setVisibility(View.VISIBLE);
        }

        this.layoutLoading = findViewById(R.id.loadingLayout);
        this.layoutLoading.setVisibility(View.GONE);

        // Badge number
        this.editTextBadgeNumber = findViewById(R.id.badgeNumberEditText);
        this.editTextBadgeNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                hideHelpOverlay();

                if (!hasFocus) {
                    // Badge number
                    String badgeNumber = editTextBadgeNumber.getText().toString();
                    String badgeRegex = "^[0-9]{9}$";
                    Pattern badgePattern = Pattern.compile(badgeRegex);
                    Matcher badgeMatcher = badgePattern.matcher(badgeNumber);
                    if (TextUtils.isEmpty(badgeNumber)) {   // Check if empty
                        return;
                    } else if (!badgeMatcher.find()) {      // Check if valid badge number
                        editTextBadgeNumber.setError("The badge number should consist of nine digits.");
                    }
                }
            }
        });

        // License plate
        this.editTextLicensePlate = findViewById(R.id.licensePlateEditText);
        this.editTextLicensePlate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                hideHelpOverlay();

                if (!hasFocus) {
                    // State selection; false if still on default value of "State*"
                    if (spinnerState.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerState.getSelectedView()).setError("Please select a state.");
                    }

                    // License plate
                    // Check if empty
                    String licensePlateNumber = editTextLicensePlate.getText().toString().toLowerCase();

                    boolean stateValidation = plateValidbyState(licensePlateNumber);

                    if (!stateValidation) { // Check if valid value license plate number
                        editTextLicensePlate.setError("The license plate number does not match the state's specified format." +
                                "If it is a vanity plate, please select CUSTOM in the drop down.");
                    }

                }
            }
        });

        this.spinnerState = findViewById(R.id.stateSpinner);

        // Assign values for state selector
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.state_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinnerState.setAdapter(adapter);

        this.textViewColors = findViewById(R.id.colorTextView);
        this.textViewColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.requestFocus();
            }
        });

        // Colors
        this.layoutColors = findViewById(R.id.colorChoicesLayout);

        // Multiple vehicles
        this.switchMultiVehicle = findViewById(R.id.multipleVehiclesSwitch);
        this.switchMultiVehicle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                hideHelpOverlay();
                switchMultiVehicle.setOnClickListener(null);
            }
        });

        // EV
        this.switchEV = findViewById(R.id.evSwitch);
        this.switchEV.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                hideHelpOverlay();
                switchEV.setOnClickListener(null);
            }
        });

        // Handicap slider and image holder
        this.textViewAddImageInstructions = findViewById(R.id.handicapAddImageInstructionTextView);

        // Toggle whether or not handicap
        this.switchHandicap = findViewById(R.id.handicapSwitch);
        this.switchHandicap.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                hideHelpOverlay();

                if(isChecked) {
                    textViewAddImageInstructions.setVisibility(View.VISIBLE);
                    btnAddImage.setVisibility(View.VISIBLE);
                } else {
                    textViewAddImageInstructions.setVisibility(View.GONE);
                    btnAddImage.setVisibility(View.GONE);
                    imageView1.setVisibility(View.GONE);
                    images.clear();
                }
            }
        });

        // Button to add image of handicap badge/icon
        this.btnAddImage = findViewById(R.id.addHandicapImageButton);
        this.btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideHelpOverlay();

                if (imageView1.getVisibility() == View.VISIBLE) {
                    Toast.makeText(ParkingPassTicketForm.this, "You can only add 1 image.", Toast.LENGTH_SHORT).show();
                } else {
                    addImage();
                }
            }
        });

        // Image of handicap badge/icon
        this.imageView1 = findViewById(R.id.handicapImageView);
        this.imageView1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView1.setImageBitmap(null);
                imageView1.setVisibility(View.GONE);
                images.remove(1);
            }
        });

        // Preferred contact method
        this.layoutContactMethod = findViewById(R.id.contactToggleLayout);

        // Set default contact method as email
        ImageButton emailContactOption = findViewById(R.id.contactEmailToggle);
        emailContactOption.setActivated(true);

        // Alternate contact information
        this.editTextContactInfo = findViewById(R.id.altContactInfoEditText);
        this.editTextContactInfo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                hideHelpOverlay();

                if (!hasFocus) {
                    // Contact information
                    String emailRegex = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
                    Pattern emailPattern = Pattern.compile(emailRegex);
                    Matcher emailMatcher = emailPattern.matcher(editTextContactInfo.getText().toString());

                    String prefContactMethod = getActivatedViewValue(layoutContactMethod);

                    String phoneRegex = "^[\\d]{10}$";
                    Pattern phonePattern = Pattern.compile(phoneRegex);
                    Matcher phoneMatcher = phonePattern.matcher(editTextContactInfo.getText().toString());

                    if (TextUtils.isEmpty(editTextContactInfo.getText().toString())) {
                        return;
                    } else if(prefContactMethod.equals("Email") && !emailMatcher.matches()){
                        editTextContactInfo.setError("Please enter a valid email.");
                    } else if((prefContactMethod.equals("Call") || prefContactMethod.equals("Message")) && !phoneMatcher.matches()){
                        editTextContactInfo.setError("Please enter a valid phone number.");
                    }
                }
            }
        });
    }
}