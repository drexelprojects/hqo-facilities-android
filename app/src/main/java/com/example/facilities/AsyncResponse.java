/**
 * AsyncResponse.java
 * Interface for giving tasks the ability to handle {@code AsyncTask} responses upon execute finish.
 */
package com.example.facilities;

import org.json.JSONObject;

public interface AsyncResponse {
    void processFinish(JSONObject result);
}
