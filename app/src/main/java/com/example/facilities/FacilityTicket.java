/**
 * FacilityTicket.java
 * Represents an application for a facility ticket.
 */
package com.example.facilities;

import android.graphics.Bitmap;

import java.util.HashMap;

public class FacilityTicket extends Ticket {
    private String description;
    private Location location;
    private boolean isNearMissOpportunity;
    private boolean isCompleted;
    private HashMap<Integer, Bitmap> images = new HashMap<>();

    /**
     * Constructor for {@code FacilityTicket}.
     * @param description Description of the request.
     * @param location Location of the request.
     */
    public FacilityTicket(String description, Location location) {
        super("TYPE_FACILITY");

        this.description = description;
        this.location = location;
    }

    /**
     * Constructo for {@code FacilityTicket}.
     * @param description Description of the request.
     */
    public FacilityTicket(String description) {
        this(description, null);
    }

    /**
     * @return Description of the request.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description of the request.
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Location of the request. Can contain room, floor, building, and details of location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @return Images attached to request.
     */
    public HashMap<Integer, Bitmap> getImages() {
        return images;
    }

    /**
     * Sets images attached to request.
     * @param images {@code HashMap} containing images in {@code Bitmap} format.
     */
    public void setImages(HashMap<Integer, Bitmap> images) {
        this.images = images;
    }

    /**
     * @return True if is a "near miss opportunity", false otherwise.
     */
    public boolean isNearMissOpportunity() {
        return isNearMissOpportunity;
    }

    /**
     * Sets whether or not the request is a "near miss opportunity".
     * @param isNearMissOpportunity
     */
    public void setNearMissOpportunity(boolean isNearMissOpportunity) {
        this.isNearMissOpportunity = isNearMissOpportunity;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean isCompleted) {
        this.isCompleted = isCompleted;
    }
}
