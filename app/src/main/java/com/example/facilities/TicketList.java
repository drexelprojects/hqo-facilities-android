/**
 * TicketList.java
 * Displays list of {@code Ticket} objects to user, ordered by date of creation. Utilizes a
 * {@code LinkedHashMap} to store the tickets for the purpose of simulating a caching system that
 * will allow the application to load the tickets without needing to reparse all the data retrieved.
 */
package com.example.facilities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.content.Intent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TicketList extends AppCompatActivity implements TicketAdapter.ItemClicked, AsyncResponse {

    public static final int FACILITY_TICKET_FORM_REQUEST = 1;
    public static final int PARKING_PASS_TICKET_FORM_REQUEST = 2;

    private static Map<Integer, Ticket> tickets = new LinkedHashMap<>();

    private RecyclerView recyclerView;
    private LayoutManager layoutManager;
    private Adapter ticketAdapter;
    private ProgressBar loadingProgressBar;

    private DialogThankYou dialogThankYou;
    private DialogFragment dialogError;
    private ConstraintLayout helpOverlayLayout;

    private ConstraintLayout enlargenedImageLayout;
    private ImageView enlargenedImageView;

    private ImageButton parkingPassButton, helpButton;
    private FloatingActionButton facilityTicketButton;

    private static final String TAG = "TICKET_LIST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_list);

        this.initWidgets();
        this.retrieveTickets();
    }



    /**
     * Starts the async task to retrieve the tickets.
     */
    private void retrieveTickets(){
        // Show loader while it is retrieving
        this.loadingProgressBar.setVisibility(View.VISIBLE);

        // Set the endpoint and start the task
        String endpoint = "https://modular-ground-255216.appspot.com/get_tickets?user_id=vjt33";
        TaskDatabaseRetrieve task = new TaskDatabaseRetrieve(this, this, endpoint);
        task.execute();
    }

    /**
     * Sorts tickets by creation date. Converts to a {@code List} to sort, then reinserts into
     * a now cleared map since a {@code LinkedHashMap} is ordered by insertion.
     */
    private void sortTickets() {
        // Create list from entry set to then be sorted
        List<Map.Entry<Integer, Ticket>> ticketList = new ArrayList<>(tickets.entrySet());
        Collections.sort(ticketList, new Comparator<Map.Entry<Integer, Ticket>>() {
            // Custom comparison operation for the list
            public int compare(Map.Entry<Integer, Ticket> ticketA, Map.Entry<Integer, Ticket> ticketB) {
                // Reverse order (newest to oldest)
                return (-1)*ticketA.getValue().compareTo(ticketB.getValue());
            }
        });

        // Clear the map and insert the values in the correct order
        this.tickets.clear();
        for (Map.Entry<Integer, Ticket> ticket : ticketList) {
            this.tickets.put(ticket.getKey(), ticket.getValue());
        }
    }

    /**
     * Handles result from async task(s) ran in {@code retrieveTickets()}. Parses data for parking passes
     * and ticets and inserts them into {@code tickets}. Then, it sorts the tickets using {@code sortTickets()}.
     * Once sorted, notifies the {@code ticketAdapter} that the data set has changed so that the {@code RecyclerView}
     * can update accordingly.
     * @param result Response from the async task.
     */
    @Override
    public void processFinish(JSONObject result) {
        try{
            JSONArray parkingPassTickets = result.getJSONArray("parking_passes");
            JSONArray facilityTickets = result.getJSONArray("tickets");

            // Parking pass tickets
            for (int i = 0; i < parkingPassTickets.length(); i++){
                JSONObject data = new JSONObject(parkingPassTickets.get(i).toString());

                // Check if map already contains ticket; if so do not bother adding
                int id = data.getInt("id");
                if (tickets.containsKey(id) && tickets.get(id).getType().equals("TYPE_PARKING_PASS")) {
                    continue;
                }

                // Form details
                String badgeNumber = data.getString("badge_no");
                String firstName = data.getString("first_name");
                String lastName = data.getString("last_name");
                String email = data.getString("email");
                boolean isMultiple = data.getBoolean("multiple");
                boolean isHandicap = data.getBoolean("is_handicap");
                String prefContact = data.getString("pref_contact");
                String altContact = data.getString("alt_contact");
                boolean isContractor = data.getBoolean("is_contractor");

                // Details for vehicle
                String make = data.getString("make");
                String model = data.getString("model");
                int year = data.getInt("year");
                boolean isEV = data.getBoolean("is_ev");
                String color = data.getString("color");

                // Create the vehicle
                Vehicle vehicle = new Vehicle(make, model, year, isEV, color);

                // Details for registered vehicle
                String licensePlate = data.getString("license_plate");
                String state = data.getString("state");

                // Create the registered vehicle
                RegisteredVehicle registeredVehicle = new RegisteredVehicle(vehicle, licensePlate, state);

                // Construct the ticket
                ParkingPassTicket ticket = new ParkingPassTicket(badgeNumber, firstName, lastName, email, registeredVehicle, isMultiple, isHandicap, prefContact, altContact, isContractor);

                // Handicap image, if applicable
//                HashMap<Integer, Bitmap> images = new HashMap<>();
//                String image = data.getString("handicap_image");
//                if (image != null && !image.isEmpty()) {
//                    ImageConverter converter = new ImageConverter();
//                    images.put(1, converter.convertStringToBitmap(image));
//                    ticket.setImages(images);
//                }

                // Retrieve creation date and set the time
                String creationDate = data.getString("creation_date");
                Date date = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss zzz").parse(creationDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                ticket.setCreationDate(calendar);

                this.tickets.put(id, ticket);
            }

            // Facility tickets
            for (int i = 0; i < facilityTickets.length(); i++){
                JSONObject data = new JSONObject(facilityTickets.get(i).toString());

                // Check if map already contains ticket; if so do not bother adding
                int id = data.getInt("id");
                if (tickets.containsKey(id) && tickets.get(id).getType().equals("TYPE_FACILITY")) {
                    continue;
                }

                String description = data.getString("description");

                Location location = null;
                try {
                    JSONObject locationJSON = data.getJSONObject("location_details");
                    if (locationJSON != null) {
                        String room = "", floor = "", building = "", desc = "";
                        if (locationJSON.has("room")) {
                            room = locationJSON.getString("room");
                        }
                        if (locationJSON.has("floor")) {
                            floor = locationJSON.getString("floor");
                        }
                        if (locationJSON.has("building")) {
                            building = locationJSON.getString("building");
                        }
                        if (locationJSON.has("desc")) {
                            desc = locationJSON.getString("desc");
                        }

                        location = new Location(room, floor, building, desc);
                    }
                } catch (JSONException e) {
                    Log.d(TAG, e.toString());
                }

                FacilityTicket ticket = new FacilityTicket(description, location);

                boolean isNearMissOpportunity = data.getBoolean("is_near_miss_opportunity");
                ticket.setNearMissOpportunity(isNearMissOpportunity);

                boolean isCompleted = data.getBoolean("is_completed");
                ticket.setCompleted(isCompleted);

                // Images
                JSONArray imagesArray = data.getJSONArray("images");
                HashMap<Integer, Bitmap> images = new HashMap<>();
                for (int j=0; j< imagesArray.length(); j++) {
                    String image = imagesArray.getString(j);
                    ImageConverter converter = new ImageConverter();
                    images.put(j + 1, converter.convertStringToBitmap(image));
                    ticket.setImages(images);
                }

                String creationDate = data.getString("creation_time");
                Date date = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss zzz").parse(creationDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                ticket.setCreationDate(calendar);

                this.tickets.put(id, ticket);
            }

            // Sort the tickets by creation date
            this.sortTickets();

            // Notify the adapter that the data set has changed so that the recycler view can update accordingly.
            this.ticketAdapter.notifyDataSetChanged();

            // Hide spinner as the process has finished.
            this.loadingProgressBar.setVisibility(View.GONE);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    /**
     * Navigate to facility ticket form activity.
     * @param view The calling view
     */
    public void navTicketForm(View view) {
        Intent intent = new Intent(TicketList.this, FacilityTicketForm.class);
        startActivityForResult(intent, FACILITY_TICKET_FORM_REQUEST);
    }

    /**
     * Navigate to parking pass form activity
     * @param view The caling view
     */
    public void navParkingPassForm(View view) {
        Intent intent = new Intent(this, ParkingPassTicketForm.class);
        startActivityForResult(intent, PARKING_PASS_TICKET_FORM_REQUEST);
    }

    // After FacilityTicketForm.class is submitted, it will return here
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == FACILITY_TICKET_FORM_REQUEST && resultCode == RESULT_OK){
            // Update ticket list and show thank you for facility ticket submission
            this.retrieveTickets();
            super.onActivityResult(requestCode, resultCode, data);
            this.dialogThankYou.showAndGetType(getSupportFragmentManager(), "ticketThankYou", FACILITY_TICKET_FORM_REQUEST);

        } else if (requestCode == PARKING_PASS_TICKET_FORM_REQUEST && resultCode == RESULT_OK) {
            // Update ticket list and show thank you for parking pass application submission
            this.retrieveTickets();
            super.onActivityResult(requestCode, resultCode, data);
            this.dialogThankYou.showAndGetType(getSupportFragmentManager(), "ticketThankYou", PARKING_PASS_TICKET_FORM_REQUEST);

        } else if(requestCode == FACILITY_TICKET_FORM_REQUEST && resultCode == RESULT_FIRST_USER){
            // Retrieve tickets again just in case, but show error dialog
            this.retrieveTickets();
            super.onActivityResult(requestCode, resultCode, data);
            this.dialogError.show(getSupportFragmentManager(), "ticketError");
        }
    }

    /**
     * Enlarges an image for viewing purposes.
     * @param view The calling {@code ImageView}
     */
    public void enlargeImage(View view) {
        ImageView image = (ImageView) view;

        // Retrieve bitmap from image
        BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        // Set bitmap for enlarged image
        this.enlargenedImageView.setImageBitmap(bitmap);

        // Show the overlay
        this.enlargenedImageLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Closes the viewing of the enlarged image.
     * @param view The calling {@code ImageView}
     */
    public void closeImage(View view) {
        this.enlargenedImageLayout.setVisibility(View.GONE);
    }

    /**
     * Displays the help overlay and
     * @param view
     */
    public void toggleHelp(View view){
        if(helpOverlayLayout.getVisibility() == View.VISIBLE){
            FacilityApp.tutorial.reset(true);
            helpOverlayLayout.setVisibility(View.GONE);
        } else if(helpOverlayLayout.getVisibility() == View.GONE){
            FacilityApp.tutorial.reset(false);
            helpOverlayLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Hides the help overlay if it is currently visible
     */
    private void hideHelpOverlay() {
        if (helpOverlayLayout.getVisibility() == View.VISIBLE) {
            helpOverlayLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Initializes widget(s) from the layout file.
     */
    private void initWidgets() {
        this.helpOverlayLayout = findViewById(R.id.helpOverlay);
        this.helpOverlayLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
            }
        });
        if (!FacilityApp.tutorial.shown(getString(R.string.pref_ticket_list))) {
            this.helpOverlayLayout.setVisibility(View.VISIBLE);
        }

        this.helpButton = findViewById(R.id.helpButton);
        this.helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleHelp(view);
            }
        });

        this.parkingPassButton = findViewById(R.id.parkingPassButton);
        this.parkingPassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FacilityApp.tutorial.shown(getString(R.string.pref_ticket_list));
                navParkingPassForm(view);
            }
        });

        this.facilityTicketButton = findViewById(R.id.createTicket);
        this.facilityTicketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FacilityApp.tutorial.shown(getString(R.string.pref_ticket_list));
                navTicketForm(view);
            }
        });

        this.recyclerView = findViewById(R.id.list);
        this.recyclerView.setHasFixedSize(true);

        this.layoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(layoutManager);

        this.ticketAdapter = new TicketAdapter(TicketList.this, this.tickets);
        this.recyclerView.setAdapter(ticketAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        this.recyclerView.addItemDecoration(dividerItemDecoration);

        this.ticketAdapter.notifyDataSetChanged();

        this.loadingProgressBar = findViewById(R.id.loadingProgressBar);

        this.dialogThankYou = new DialogThankYou();
        this.dialogError = new DialogError();

        this.enlargenedImageLayout = findViewById(R.id.enlargenedImageLayout);
        this.enlargenedImageView = findViewById(R.id.enlargenedImageView);
    }

    /**
     * Close the thank you dialog.
     * @param view View that triggered the event.
     */
    public void closeDialog(View view) {
        this.dialogThankYou.dismiss();
    }

    /**
     * Close the error dialog.
     * @param view View that triggered the event.
     */
    public void closeDialog_error(View view){
        this.dialogError.dismiss();
    }

    @Override
    public void onItemClicked(int index) { }
}
