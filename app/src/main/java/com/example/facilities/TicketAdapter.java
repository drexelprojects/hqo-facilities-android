/**
 * TicketAdapter.java
 * {@code Adapter} for the ticket list {@code RecyclerView}.
 */
package com.example.facilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Map;

public class TicketAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ItemClicked activity;
    private Map<Integer, Ticket> tickets;

    private static int TYPE_FACILITY = 1;
    private static int TYPE_PARKING_PASS = 2;

    /**
     * Constructor for {@code TicketAdapter}.
     * @param context {@code Activity} that the {@code RecyclerView} is operating in.
     * @param list {@code Map} containing list of tickets. Used as the source of data
     *                        for the {@code RecyclerView}.
     */
    public TicketAdapter(Context context, Map<Integer, Ticket> list){
        this.activity = (TicketAdapter.ItemClicked) context;
        this.tickets = list;
    }

    /**
     * Interface for items being clicked.
     */
    public interface ItemClicked{
        void onItemClicked(int index);
    }

    /**
     * Retrieves the current item count for {@code tickets}.
     * @return Count of {@code tickets}.
     */
    @Override
    public int getItemCount() {
        return tickets.size();
    }

    /**
     * Retrieves the item view type. The item view type is based on the ticket types.
     * Used so the respective ViewHolder can be used when initializing the view(s). Sets
     * the type of the ticket accordingly so that the correct {@code ViewHolder} can be used.
     *
     * @param position Index of item in list.
     * @return Type of ticket. Returns 0 if it does not match any valid values;
     */
    @Override
    public int getItemViewType(int position) {
        ArrayList<Integer> keys = new ArrayList(tickets.keySet());
        Ticket ticket = tickets.get(keys.get(position));
        String ticketType = ticket.getType();

        int type;
        switch(ticketType) {
            case "TYPE_FACILITY":
                type = TYPE_FACILITY;
                break;
            case "TYPE_PARKING_PASS":
                type = TYPE_PARKING_PASS;
                break;
            default:
                type = 0;
                break;
        }

        return type;
    }

    /**
     * Sets the {@code ViewHolder} to be used for a {@code Ticket}.
     * @param parent Parent view of the facility ticket layout.
     * @param viewType Type of {@code ViewHolder} to retrieve.
     * @return Type of {@code Ticket}.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;

        View view = null;
        switch (viewType) {
            case 1:
                // Facility ticket view holder
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_facility, parent, false);
                viewHolder = new FacilityTicketViewHolder(view);
                break;

            case 2:
                // Parking pass view holder
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_parking_pass, parent, false);
                viewHolder = new ParkingPassTicketViewHolder(view);
                break;

            default:
                viewHolder = null;
                break;
        }

        return viewHolder;
    }

    /**
     * Bind the {@code Ticket} to the {code ViewHolder}.
     * @param holder {@code ViewHolder} that will accept the corresponding {@code Ticket} type for rendering.
     * @param position Position of the {@code Ticket} in the list {@code tickets}.
     */
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ArrayList<Integer> keys = new ArrayList(tickets.keySet());
        switch (holder.getItemViewType()) {
            case 1:
                // Initialize view holder
                FacilityTicketViewHolder facilityTicketViewHolder = (FacilityTicketViewHolder) holder;
                FacilityTicket facilityTicket = (FacilityTicket) tickets.get(keys.get(position));

                // Bind values
                facilityTicketViewHolder.bind(facilityTicket);
                break;

            case 2:
                // Initialize view holder
                ParkingPassTicketViewHolder parkingPassTicketViewHolder = (ParkingPassTicketViewHolder) holder;
                ParkingPassTicket parkingPassTicket = (ParkingPassTicket) tickets.get(keys.get(position));

                // Bind values
                parkingPassTicketViewHolder.bind(parkingPassTicket);
                break;

            default:
                break;
        }
    }
}
