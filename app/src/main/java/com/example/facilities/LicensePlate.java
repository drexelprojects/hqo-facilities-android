/**
 * LicensePlate.java
 * License plate for a registered vehicle in the US. All US license plates have a
 * license plate "number" (sequence of alphanumeric characters that differs from state to state)
 * and an associated state of registration.
 */
package com.example.facilities;

public class LicensePlate {
    private final String licensePlateNumber;
    private final String state;

    /**
     * Constructor for {@code LicensePlate}.
     * @param licensePlateNumber
     * @param state
     */
    public LicensePlate(String licensePlateNumber, String state) {
        this.licensePlateNumber = licensePlateNumber;
        this.state = state;
    }

    /**
     * @return License plate number
     */
    public String getLicensePlateNumber() {
        return this.licensePlateNumber;
    }

    /**
     * @return State of vehicle registration.
     */
    public String getState() {
        return this.state;
    }
}
