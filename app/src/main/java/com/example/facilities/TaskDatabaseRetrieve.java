/**
 * TaskDatabaseSubmit.java
 * Performs an {@code AsynTask} and then processes the result through {@code delegate} in {@code onPostExecute(...)}.
 * By processing the result this way, we can achieve a synchronized result from an asynchronous task.
 *
 * Retrieves data from the database.
 */
package com.example.facilities;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

public class TaskDatabaseRetrieve extends AsyncTask<JSONObject, String, JSONObject> {
    private String TAG = "TASK_DATABASE_RETRIEVE";

    private Context context;
    private AsyncResponse delegate;
    private String endpoint;

    private String result;

    /**
     * Constructor for {@code TaskDatabaseRetrieve}.
     * @param context Conext that the {@code AsyncTask} should operate in.
     * @param delegate Delegate that will process the result once the task is completed.
     * @param endpoint Endpoint to send the volley request to.
     */
    public TaskDatabaseRetrieve(Context context, AsyncResponse delegate, String endpoint) {
        this.context = context;
        this.delegate = delegate;
        this.endpoint = endpoint;
    }

    /**
     * Performs the operation in the backgrund (non-UI thread).
     * @param params Optional parameters if needed when sending database request.
     * @return Tickets retrieved from database.
     */
    @Override
    public JSONObject doInBackground(JSONObject...params) {
        VolleyRequestJSON volley = new VolleyRequestJSON(this.endpoint);

        return volley.get(this.context);
    }

    /**
     * Send results to delegate upon task finishin.
     * @param result Results to send for processing.
     */
    @Override
    protected void onPostExecute(JSONObject result) {
        this.delegate.processFinish(result);
    }
}
