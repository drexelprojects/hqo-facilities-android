package com.example.facilities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;

/**
 * Activity to scan license plate and detect the text
 * Returns the text to ParkingPassTicketForm
 */
public class ScanLicensePlateActivity extends AppCompatActivity {
    private SurfaceView surfaceView;
    private CameraSource cameraSource;
    private SparseArray<TextBlock> textBlock = new SparseArray<>();
    private TextView textView;
    private Button button;

    private static final String TAG = "ScanLicensePlate";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_license_plate);

        initWidgets();
        detectText();
    }

    /**
     * Navigate back to the ParkingPassTicketForm by killing this activity
     * @param view The calling view
     */
    public void navParkingPassTicketForm(View view) {
        ScanLicensePlateActivity.this.finish();
    }

    /**
     * Initialize views and widgets needed for this activity
     */
    public void initWidgets() {
        surfaceView = findViewById(R.id.scanLicensePlateSurfaceView);
        textView = findViewById(R.id.scanLicensePlateTextViewResult);
        button = findViewById(R.id.scanLicensePlateButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendLicensePlateToParkingPass(textView.getText().toString());
            }
        });
    }

    /**
     * Set the text recognizer and build the camera source
     */
    private void detectText() {
        // Create the TextRecognizer
        final TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();

        if (!textRecognizer.isOperational()) {
            // Note: The first time that an app using a Vision API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any text,
            // barcodes, or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            Log.w(TAG, "Detector dependencies not loaded yet");
        } else {

            // Initialize cameraSource and set Autofocus on.
            cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setAutoFocusEnabled(true)
                    .build();

            // Add call back to SurfaceView and check if camera permission is granted.
            // If permission is granted we can start our cameraSource and pass it to surfaceView
            surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {

                        if (ActivityCompat.checkSelfPermission(ScanLicensePlateActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[] {"android.permission.CAMERA"},1);
                            return;
                        }
                        cameraSource.start(surfaceView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });

            //Set the TextRecognizer's Processor.
            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {
                }

                /**
                 * Detect all the text from the cameraSource using TextBlock and append the values into a stringBuilder
                 * Put strings into recognizedText list and send back to ParkingPassTicketForm to retrieve license plate
                 * @param detections Data structure containing TextBlock results
                 */
                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    textBlock = detections.getDetectedItems();

                    if (textBlock.size() != 0 ){

                        textView.post(new Runnable() {
                            @Override
                            public void run() {
                                StringBuilder stringBuilder = new StringBuilder();
                                for(int i = 0; i < textBlock.size(); i++) {
                                    TextBlock item = textBlock.valueAt(i);
                                    stringBuilder.append(item.getValue());
                                }
                                // set the stringBuilder value to textView
                                textView.setText(stringBuilder.toString());
                            }
                        });
                    }
                }
            });
        }
    }

    /**
     * Send the text of the license plate number to the ParkingPassTicketForm class
     * @param text Text containing license plate number
     */
    public void sendLicensePlateToParkingPass(String text) {
        Intent intent = new Intent();

        if (!text.equals("No License Plate Shown")) {
            intent.putExtra("licensePlate", text);
            setResult(RESULT_OK, intent);
            ScanLicensePlateActivity.this.finish();
        }
    }

}
