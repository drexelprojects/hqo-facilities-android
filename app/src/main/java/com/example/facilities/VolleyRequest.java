/**
 * Volley request interface for templating purposes.
 * Allows for dynamic assignment of types for volley requests,
 * based on the type of {@coode T}.
 */
package com.example.facilities;

import android.content.Context;

import org.json.JSONObject;

public interface VolleyRequest<T> {
    T get(Context context);
    T post(Context context, T obj);
}
