package com.example.facilities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

/**
 * Activity to scan a QR code and detect the result
 * Returns the result to FacilityTicketForm
 */
public class ScanQRCodeActivity extends AppCompatActivity {
    private SurfaceView surfaceView;
    private CameraSource cameraSource;
    private SparseArray<Barcode> barcodes = new SparseArray<>();

    private static final String TAG = "ScanQRCode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr_code);

        initWidgets();
        detectBarcodes();
    }

    /**
     * Navigate back to the FacilityTicketForm by killing this activity
     * @param view The calling view
     */
    public void navFacilityTicketForm(View view) {
        ScanQRCodeActivity.this.finish();
    }

    /**
     * Initialize views and widgets needed for this activity
     */
    public void initWidgets(){
        surfaceView = findViewById(R.id.scanQRCodeSurfaceView);
    }

    /**
     * Set the barcode detector and build the camera source
     */
    private void detectBarcodes() {
        // Create the BarcodeDetector
        final BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(getApplicationContext())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        if(!barcodeDetector.isOperational()){
            // Note: The first time that an app using a Vision API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any text,
            // barcodes, or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            Log.w(TAG, "Detector dependencies not loaded yet");
        } else {

            cameraSource = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                    .setAutoFocusEnabled(true)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedFps(15.0f)
                    .build();

            // Add call back to SurfaceView and check if camera permission is granted.
            // If permission is granted we can start our cameraSource and pass it to surfaceView
            surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {
                        if (ActivityCompat.checkSelfPermission(ScanQRCodeActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[] {"android.permission.CAMERA"},1);
                            return;
                        }
                        cameraSource.start(surfaceView.getHolder());
                    } catch (IOException ie) {
                        Log.w(TAG, ie.toString());
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });

            // Set the BarcodeDetector's processor
            barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
                @Override
                public void release() {

                }

                /**
                 * Detect all the barcodes from the cameraSource
                 * Send detected items back to FacilityTicketForm to retrieve location
                 * @param detections Data structure containing Barcode results
                 */
                @Override
                public void receiveDetections(Detector.Detections<Barcode> detections) {
                    barcodes = detections.getDetectedItems();

                    if (barcodes.size() != 0) {
                        barcodeDetector.release();
                        Intent intent = new Intent();
                        intent.putExtra("barcode", barcodes.valueAt(0).displayValue);
                        setResult(RESULT_OK, intent);
                        ScanQRCodeActivity.this.finish();
                    }
                }
            });
        }
    }
}
