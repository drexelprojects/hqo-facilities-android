/**
 * DialogThankYou.java
 * Dialog to thank user for their submission.
 */
package com.example.facilities;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import android.app.Dialog;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.TextView;

public class DialogThankYou extends DialogFragment {
    private int type;

    /**
     * Default constructor for {@code DialogThankYou}
     */
    public DialogThankYou() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        return view;
    }

    /**
     * Handles creation of the dialog based upon the type of request.
     * @return Created dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Set custom view for dialog
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dialog_thank_you, null);

        TextView confirmationTextView = (TextView) dialog.findViewById(R.id.confirmationTextView);

        if (type == 2) {
            confirmationTextView.setText(getString(R.string.dialog_parking_pass_confirmation));
        } else {
            confirmationTextView.setText(getString(R.string.dialog_email_confirmation));
        }

        builder.setView(dialog);

        return builder.create();
    }

    /**
     * Shows dialog based on passed in {@code FragmentManager} and {@code tag}.
     * @param fragmentManager Mangages the dialog fragment.
     * @param tag Result of request (error, succes, etc.)
     * @param type Type of request (parking pass, facility ticket, etc.)
     */
    public void showAndGetType(FragmentManager fragmentManager, String tag, int type) {
        this.type = type;
        this.show(fragmentManager, tag);
    }
}
