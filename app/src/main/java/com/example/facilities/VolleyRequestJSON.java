/**
 * VolleyRequestJSON.java
 * Implementation of {@code VolleyRequest<T>}. Gets and posts {@code JSONObject} requests.
 */
package com.example.facilities;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class VolleyRequestJSON implements VolleyRequest<JSONObject> {
    private String TAG = "VOLLEY_JSON";
    private String url;

    public VolleyRequestJSON(String url) {
        this.url = url;
    }

    /**
     * Utilizes {@code RequestFuture<>} to make a {@code JSONObjectRequest} GET.
     * Queues the request within the context dictated by parameter {@code context}.
     * @param context Context to perform the request in.
     * @return Response of the request.
     */
    @Override
    public JSONObject get(Context context) {
        JSONObject response = null;
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(url, null, future, future);
        requestQueue.add(request);

        try {
            response = future.get();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        return response;
    }

    /**
     * Utilizes {@code RequestFuture<>} to make a {@code JSONObjectRequest} POST.
     * Queues the request within the context dictated by parameter {@code context}.
     * @param context Context to perform the request in.
     * @param obj Payload to be posted in the request.
     * @return Response of the request.
     */
    @Override
    public JSONObject post(Context context, JSONObject obj) {
        final JSONObject data = obj;
        JSONObject response = null;
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(url, obj, future, future) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return data == null ? null : data.toString().getBytes();
                } catch (Exception e){
                    Log.d(TAG, e.toString());
                    return null;
                }
            }
        };

        requestQueue.add(request);

        try  {
            response = future.get();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        return response;
    }
}
