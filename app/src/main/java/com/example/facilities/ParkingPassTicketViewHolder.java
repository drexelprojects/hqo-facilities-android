/**
 * ParkingPassTicketViewHolder.java
 * {@code ViewHolder} for a {@code ParkingPassTicket} to be displayed on the {@code RecyclerView} of
 * the {@code TicketList}.
 */
package com.example.facilities;

import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;

public class ParkingPassTicketViewHolder extends RecyclerView.ViewHolder {
    private final float DEFAULT_HEIGHT = 90.0f;

    private TextView detailDateTextView, detailName, detailLicense, detailBadgeNumber, detailCar, detailCarLabel;
    private ImageView detailColor;

    /**
     * Constructor for {@code ParkingPassTicketViewHolder}
     * @param itemView Layout view to bind values to.
     */
    public ParkingPassTicketViewHolder(View itemView) {
        super(itemView);

        this.initWidget();
    }

    public void bind(ParkingPassTicket parkingPassTicket) {
        // Set view item values
        this.itemView.setTag(parkingPassTicket);

        SimpleDateFormat format = new SimpleDateFormat("dd MMM YYYY '@' hh:mm a");
        String date = format.format(parkingPassTicket.getCreationDate().getTime());
        this.detailDateTextView.setText(date);

        this.detailName.setText(parkingPassTicket.getFirstName() + " " + parkingPassTicket.getLastName());
        this.detailName.setSingleLine(true);
        this.detailLicense.setText(parkingPassTicket.getLicensePlate());

        String carDetailText = "";
        if (parkingPassTicket.getYear() != 0 && parkingPassTicket.getYear() != 1) {
            carDetailText += parkingPassTicket.getYear();
        }
        if (!parkingPassTicket.getMake().isEmpty()) {
            if (!carDetailText.isEmpty()) {
                carDetailText += " ";
            }
            carDetailText += parkingPassTicket.getMake();
        }
        if (!parkingPassTicket.getModel().isEmpty()) {
            if (!carDetailText.isEmpty()) {
                carDetailText += " ";
            }
            carDetailText += parkingPassTicket.getModel();
        }

        if (carDetailText.isEmpty()) {
            this.detailCar.setVisibility(View.GONE);
            this.detailCarLabel.setVisibility(View.GONE);
        } else {
            this.detailCar.setText(carDetailText);
        }

        this.detailBadgeNumber.setText(parkingPassTicket.getBadgeNumber());

        String color = parkingPassTicket.getColor();
        ColorDrawable background = new ColorDrawable(Color.parseColor(color));
        this.detailColor.setBackground(background);
        this.detailColor.setContentDescription(color);
    }

    /**
     * Initialize widget(s) from layout file and set any required event listeners.
     */
    private void initWidget() {
        this.detailDateTextView = this.itemView.findViewById(R.id.detailDateTextView);
        this.detailName = this.itemView.findViewById(R.id.detailName);
        this.detailLicense = this.itemView.findViewById(R.id.detailLicenseTextView);
        this.detailBadgeNumber = this.itemView.findViewById(R.id.detailBadgeTextView);
        this.detailCar = this.itemView.findViewById(R.id.detailCarTextView);
        this.detailCarLabel = this.itemView.findViewById(R.id.detailCarLabel);
        this.detailColor = this.itemView.findViewById(R.id.detailColorImageView);

        this.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutParams params = view.getLayoutParams();

                if (view.isActivated()) {
                    // Deactivate and adjust root view
                    view.setActivated(false);
                    float density = view.getContext().getResources().getDisplayMetrics().density;
                    params.height = (int) (DEFAULT_HEIGHT * density);

                    // Detail-specific adjustments
                    detailName.setMaxLines(1);
                } else {
                    // Activate and adjust root view
                    view.setActivated(true);
                    params.height = LayoutParams.WRAP_CONTENT;

                    // Detail-specific adjustments
                    detailName.setMaxLines(Integer.MAX_VALUE);
                }

                view.setLayoutParams(params);
            }
        });
    }
}
