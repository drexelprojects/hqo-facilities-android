/**
 * Tutorial.java
 * Handles preference storing of tutorial overlays.
 */
package com.example.facilities;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

public class Tutorial {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public Tutorial(Context context, String...keys) {
        this.preferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_file), Context.MODE_PRIVATE);
        this.editor = this.preferences.edit();

        for (int i=0; i<keys.length; i++) {
            String key = keys[i];
            boolean value = this.preferences.getBoolean(key, false);
            this.editor.putBoolean(key, value);
            this.editor.apply();
        }
    }

    /**
     * Checks to see if first time opening app after install
     * Will display help overlay by default
     * @return True if tutorial shown, false otherwise
     */
    public boolean shown(String key){
        boolean shown = this.preferences.getBoolean(key, false);

        if(!shown){
            this.editor.putBoolean(key, true);
            this.editor.apply();
        }

        return shown;
    }

    /**
     * Resets all tutorials to passed in value
     * @param value If true, will hide overlays, otherwise it will show
     */
    public void reset(boolean value) {
        Map<String, ?> all = this.preferences.getAll();

        for (Map.Entry<String, ?> entry : all.entrySet()) {
            this.editor.putBoolean(entry.getKey(), value);
            this.editor.apply();
        }
    }
}
