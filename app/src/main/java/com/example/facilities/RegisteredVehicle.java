/**
 * RegisteredVehicle.java
 * Represents a standard vehicle with a registered license plate.
 */
package com.example.facilities;

public class RegisteredVehicle extends Vehicle {
    private LicensePlate licensePlate;

    private final String TAG = "RegisteredVehicle";

    /**
     * Default constructor for {@code RegisteredVehicle}.
     */
    public RegisteredVehicle() {
        this.licensePlate = null;
    }

    /**
     * Constructor for {@code RegisteredVehicle}.
     * @param make
     * @param model
     * @param year
     * @param isElectric
     * @param color
     * @param licensePlateNumber
     * @param state
     */
    public RegisteredVehicle(String make, String model, int year, boolean isElectric, String color, String licensePlateNumber, String state) {
        super(make, model, year, isElectric, color);
        this.licensePlate = new LicensePlate(licensePlateNumber, state);
    }

    /**
     * Constructor for {@code RegisteredVehicle}.
     * @param vehicle Vehicle in unregistered state. Utilizes parent copy constructor.
     * @param licensePlateNumber License plate number of the vehicle.
     * @param state State that the car is registered to.
     */
    public RegisteredVehicle(Vehicle vehicle, String licensePlateNumber, String state) {
        super(vehicle);
        this.licensePlate = new LicensePlate(licensePlateNumber, state);
    }

    /**
     * Retrieve copy of the license plate for the {@code RegisteredVehicle}.
     * @return Copy of the {@code LicensePlate} object.
     */
    public LicensePlate getLicensePlate() {
        return new LicensePlate(licensePlate.getLicensePlateNumber(), licensePlate.getState());
    }
}
