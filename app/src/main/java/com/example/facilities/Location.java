/**
 * Location.java
 * Represents a location for the subject of a facility ticket request.
 */
package com.example.facilities;

public class Location {
    private final String room;
    private final String floor;
    private final String building;
    private final String description;

    /**
     * Constructor for {@code Location}.
     * @param room Room of the location (e.g. 200-10).
     * @param floor Floor of the location (e.g. 2)
     * @param building Building (e.g. Camden HQ)
     * @param description Description of the subject (e.g. Coffee machine).
     */
    public Location(String room, String floor, String building, String description) {
        this.room = room;
        this.floor = floor;
        this.building = building;
        this.description = description;
    }

    /**
     * @return Room of the location.
     */
    public String getRoom() { return this.room; }

    /**
     * @return Floor of location.
     */
    public String getFloor() { return this.floor; }

    /**
     * @return Building of the location.
     */
    public String getBuilding() { return this.building; }

    /**
     * @return Description of the location.
     */
    public String getDescription() { return this.description; }

    /**
     * Retrieves all location details (except for {@code description}, assuming provided.
     * @return Concatenated result of combing the {@code floor}, {@code room}, and {@code building}.
     */
    public String getDetails() {
        String details = "";
        if (this.floor != null && !this.floor.isEmpty()) {
            // If floor id provided.
            details += "Floor " + this.floor;
        }

        if (this.room != null && !this.room.isEmpty()) {
            // If room is provided
            if (!details.isEmpty()) {
                details += " / ";
            }
            details += "Room " + this.room;
        }

        if (this.building != null && !this.building.isEmpty()) {
            // If building is provided
            if (!details.isEmpty()) {
                details += " / ";
            }
            details += this.building;
        }

        return details;
    }
}
