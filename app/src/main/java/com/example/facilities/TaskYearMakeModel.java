/**
 * TaskYearMakeModel.java
 * Performs an {@code AsynTask} and then processes the result through {@code delegate} in {@code onPostExecute(...)}.
 * By processing the result this way, we can achieve a synchronized result from an asynchronous task.
 *
 * Retrieves the year, make, and model from the API at {@linkplain vehicleregistrationapi.com}.
 */
package com.example.facilities;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class TaskYearMakeModel extends AsyncTask<JSONObject, String, JSONObject> {
    private String TAG = "TASK_YMM";

    // Replace with the username of the account that credits on loaded on.
    private final String VRA_USERNAME = "VTTest11";

    private Context context;
    private AsyncResponse delegate;

    /**
     * Constructor for {@code TaskYearMakeModel}.
     * @param context Conext that the {@code AsyncTask} should operate in.
     * @param delegate Delegate that will process the result once the task is completed.
     */
    public TaskYearMakeModel(Context context, AsyncResponse delegate) {
        this.context = context;
        this.delegate = delegate;
    }

    /**
     * Performs the operation in the backgrund (non-UI thread).
     * @param params Data for constructing the endpoint to send the request to, if needed.
     * @return Data containing the year, make, and model of the vehicle.
     */
    @Override
    public JSONObject doInBackground(JSONObject...params) {
        JSONObject data = params[0];
        try {
            // Get license plate number and state
            String licensePlate = data.getString("license_plate");
            String state = data.getString("state");

            // Construct URL for API and make the volley request
            String url = "https://www.regcheck.org.uk/api/reg.asmx/CheckUSA?RegistrationNumber=" + licensePlate + "&State=" + state + "&username=" + VRA_USERNAME;
            VolleyRequestString volley = new VolleyRequestString(url);
            String response = volley.get(this.context);

            // Extract XML from response
            Document document = parseStringToXML(response);
            if (document == null) {
                // If data is null, then submit blank data.
                Log.d(TAG, "Issues parsing response into XML");
                data.put("make", "");
                data.put("model", "");
                data.put("year", 0);

                // The last digits typed in the file.
                return data;
            }
            Element rootElement = document.getDocumentElement();

            // Retrieve JSON object from XML
            String vehicleJSONElement = rootElement.getElementsByTagName("vehicleJson").item(0).getTextContent();
            JSONObject obj = new JSONObject(vehicleJSONElement);

            // Retrieve needed info from JSON
            String make = obj.getJSONObject("CarMake").getString("CurrentTextValue");
            String model = obj.getJSONObject("CarModel").getString("CurrentTextValue");
            int year = Integer.parseInt(obj.getString("RegistrationYear"));

            // Add to the JSON object
            data.put("make", make);
            data.put("model", model);
            data.put("year", year);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        return data;
    }

    /**
     * Send results to delegate upon task finishin.
     * @param result Results to send for processing.
     */
    @Override
    protected void onPostExecute(JSONObject result) {
        this.delegate.processFinish(result);
    }

    /**
     * Helper method for setYearMakeModel. Converts an XML response
     * into a Document object so that the elements can be accessed.
     * @param XMLString String containing XML formatted data
     * @return XML formatted document
     */
    private Document parseStringToXML(String XMLString) {
        Document document = null;
        try{
            // Extract XML from response
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(new InputSource(new StringReader(XMLString)));
        }
        catch (Exception err){
            Log.d(TAG, "onResponse: " + err);
        }

        return document;
    }
}
