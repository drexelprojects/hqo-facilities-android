/**
 * Ticket.java
 * Abstract representation of a ticket. All tickets have a {@code type}, which can be used to
 * appropriately format the different tickets when rendering.
 */
package com.example.facilities;

import java.util.Calendar;

abstract class Ticket implements Comparable<Ticket>{
    protected final String type;
    protected Calendar creationDate;

    /**
     * Constructor for a ticket.
     * @param type Type of ticket (e.g. TYPE_FACILITY_TICKET).
     */
    public Ticket(String type) {
        this.type = type;

        Calendar calendar = Calendar.getInstance();
        this.creationDate = calendar;
    }

    /**
     * Gets the type of ticket (e.g. TYPE_FACILITY_TICKET.
     * @return Type of ticket.
     */
    public String getType() { return this.type; }

    /**
     * @return Gets the creation date of the ticket.
     */
    public Calendar getCreationDate() {
        // Utilizes copy constructor methods to avoid further manipulation.
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.creationDate.getTime());

        return calendar;
    }

    /**
     * Sets the {@code creationDate}.
     * @param calendar The date to set {@code creationDate} to.
     */
    public void setCreationDate(Calendar calendar) {
        this.creationDate.setTime(calendar.getTime());
    }

    /**
     * Comparison of two ticket objects. Utilizes the creation date
     * as the comparison field.
     * @param ticket Ticket to be compared to the calling object.
     * @return True if the calling {@code Ticket} is older than the passed in {@code Ticket}, false otherwise.
     */
    @Override
    public int compareTo(Ticket ticket) {
        return this.creationDate.compareTo(ticket.getCreationDate());
    }
}
