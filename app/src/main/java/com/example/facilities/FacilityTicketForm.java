/**
 * FacilityTicketForm.java
 * Form to submit request for a facility ticket.
 */
package com.example.facilities;

import androidx.appcompat.app.AlertDialog;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.android.volley.RequestQueue;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class FacilityTicketForm extends TicketForm implements AsyncResponse {

    private Button btnSubmit, btnAddImage, btnScanQRCode;
    private ImageButton imageBtnDescription, imageBtnClose, imageBtnDeleteImage1, imageBtnDeleteImage2, imageBtnDeleteImage3;
    private EditText editTextDescription;
    private ImageView imageView1, imageView2, imageView3;
    private SwitchCompat switchNearMissOpportunity;
    private ConstraintLayout locationLayout;
    private TextView locationDescriptionTextView, locationTextView;
    private ConstraintLayout layoutLoading;
    private ConstraintLayout helpOverlayLayout;

    private static final String TAG = "FacilityTicketForm";
    private Location location;
    private Uri photoUri;

    static final int SCAN_QR_CODE = 3;

    private HashMap<Integer, Bitmap> images = new HashMap<>();

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_form);

        this.initWidgets();
        this.checkPermission();
    }

    /**
     * Navigate to the {@code TicketList} activity.
     * @param view The calling view.
     */
    public void navTicketList(View view) {
        Intent intent = new Intent(this, TicketList.class);
        startActivity(intent);
    }

    /**
     * Checks for validity of form. Proceeds to {@code success()} if valid. Returns otherwise.
     * @param view The calling view.
     */
    public void submit(View view) {
        if (helpOverlayLayout.getVisibility() == View.VISIBLE) {
            helpOverlayLayout.setVisibility(View.GONE);
        }

        view.setEnabled(false);
        if (isValid()) {
            success();
        } else {
            view.setEnabled(true);
        }
        return;
    }

    /**
     * Checks if the form is valid.
     * @return True if valid, false otherwise.
     */
    private boolean isValid() {
        boolean isValid = true;

        String description = editTextDescription.getText().toString();
        if (TextUtils.isEmpty(description)) {
            editTextDescription.setError("You need to enter a description");
            isValid = false;
        }

        return isValid;
    }

    /**
     * Collects the form data into a {@code JSONObject} and then executes task to submit data to
     * the respective database endpoint.
     */
    private void success() {
        this.layoutLoading.setVisibility(View.VISIBLE);

        String description = editTextDescription.getText().toString();
        boolean isNearMissOpportunity = switchNearMissOpportunity.isChecked();

        try {
            JSONObject data = new JSONObject();
            data.put("user_id", "vjt33");
            data.put("description", description);
            if (this.location != null) {
                JSONObject locationJSON = new JSONObject();
                locationJSON.put("room", this.location.getRoom());
                locationJSON.put("floor", this.location.getFloor());
                locationJSON.put("building", this.location.getBuilding());
                locationJSON.put("desc", this.location.getDescription());
                data.put("location_details", locationJSON);
            } else {
                data.put("location_details", null);
            }

            JSONArray imagesArray = new JSONArray();
            for(Bitmap image : images.values()) {
                ImageConverter converter = new ImageConverter();
                String imageString = converter.convertBitmapToString(image);

                imagesArray.put(imageString);
            }
            data.put("images", imagesArray);

            data.put("is_near_miss_opportunity", isNearMissOpportunity);
            data.put("is_completed", false);

            TaskDatabaseSubmit task = new TaskDatabaseSubmit(this, this, "https://modular-ground-255216.appspot.com/create_ticket?user_id=vjt33");
            task.execute(data);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    /**
     * Processes the result of the database submission. Sets the result status
     * according to the response.
     * @param result Response of the task.
     */
    @Override
    public void processFinish(JSONObject result) {
        try {
            if (result != null && result.getBoolean("success")) {
                this.layoutLoading.setVisibility(View.GONE);
                setResult(RESULT_OK);
            } else {
                setResult(RESULT_CANCELED);
            }
            FacilityTicketForm.this.finish();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
    }

    /**
     * Check if permission has already been granted to access camera.
     * If false, disable Add Image button and request permission from user to access camera.
     */
    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                PackageManager.PERMISSION_GRANTED) {
            btnAddImage.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0);
        }
    }

    /**
     * Grant permission if user allows it. Enable Add Image button.
     * @param requestCode The request code passed in requestPermissions
     * @param permissions The array of manifest permissions
     * @param grantResults The grant results for the corresponding permissions which is either PERMISSION_GRANTED or PERMISSION_DENIED.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                btnAddImage.setEnabled(true);
            }
        }
    }

    /**
     * Opens an AlertDialog to ask user whether they want to take a photo or choose from the phone's library.
     * Starts new intents that opens camera or photo library.
     * User can click "Cancel" to close dialog.
     */
    public void addImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(FacilityTicketForm.this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    // Take photo from camera
                    dispatchTakePhotoIntent();
                } else if (items[item].equals("Choose from Library")) {
                    // Attach existing image from device library
                    Intent choosePhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    if (choosePhoto.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(choosePhoto, REQUEST_IMAGE_GALLERY);
                    }
                } else if (items[item].equals("Cancel")) {
                    // Cancel attach image action
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Save file for image and start camera intent
     */
    private void dispatchTakePhotoIntent() {
        // Take live photo
        Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePhoto.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                Log.d(TAG, e.toString());
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoUri = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
            }

            takePhoto.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(takePhoto, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * Start the ScanQRCodeActivity for location date
     * @param view The calling {@code view}
     */
    public void scanQRCode(View view) {
        hideHelpOverlay();

        Intent intent = new Intent(this, ScanQRCodeActivity.class);
        startActivityForResult(intent, SCAN_QR_CODE);
    }

    /**
     * Called when activity launched by startActivityForResult() exits.
     * @param requestCode The integer request code originally supplied to startActivityForResult(), allowing you to identify who this result came from.
     * @param resultCode The integer result code returned by the child activity through its setResult().
     * @param data An Intent, which can return result data to the caller.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                // Image from camera
                Bitmap imageBitmap = null;
                try {
                    imageBitmap = handleSamplingAndRotationBitmap(this, photoUri);
                } catch (IOException e) {
                    Log.d(TAG, e.toString());
                }
                addBitmapToImageView(imageBitmap);
            } else if (requestCode == REQUEST_IMAGE_GALLERY) {
                // Image from gallery
                if (data != null) {
                    Uri contentURI = data.getData();
                    new LoadImageDataTask(contentURI).execute();
                }
            } else if (requestCode == SCAN_QR_CODE) {
                // Scan the QR code
                if (data != null && data.getExtras() != null) {
                    String value = data.getExtras().getString("barcode");

                    try {
                        JSONObject locationJSON = new JSONObject(value);
                        String description = locationJSON.getString("desc");
                        String room = locationJSON.getString("room");
                        String floor = locationJSON.getString("floor");

                        this.location = new Location(room, floor, null, description);
                        String details = this.location.getDetails();

                        this.locationDescriptionTextView.setText(description);
                        this.locationTextView.setText(details);

                        this.locationLayout.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        Log.d(TAG, e.toString());
                    }
                }
            }
        }
    }

    /**
     * Check each image view to see if a bitmap image has already been set.
     * Set imageBitmap to image view that is not visible.
     * @param imageBitmap The bitmap image passed from onActivityResults
     */
    protected void addBitmapToImageView(Bitmap imageBitmap) {

        if (imageView1.getVisibility() == View.GONE) {

            imageView1.setImageBitmap(imageBitmap);
            imageBtnDeleteImage1.setVisibility(View.VISIBLE);
            imageView1.setVisibility(View.VISIBLE);
            images.put(1, imageBitmap);

        } else if (imageView2.getVisibility() == View.GONE &&
                imageView1.getVisibility() == View.VISIBLE) {

            imageView2.setImageBitmap(imageBitmap);
            imageBtnDeleteImage2.setVisibility(View.VISIBLE);
            imageView2.setVisibility(View.VISIBLE);
            images.put(2, imageBitmap);

        } else if (imageView3.getVisibility() == View.GONE &&
                imageView2.getVisibility() == View.VISIBLE &&
                imageView1.getVisibility() == View.VISIBLE) {

            imageView3.setImageBitmap(imageBitmap);
            imageBtnDeleteImage3.setVisibility(View.VISIBLE);
            imageView3.setVisibility(View.VISIBLE);
            images.put(3, imageBitmap);

        }
    }

    /**
     * Hides the help overlay if it is currently visible
     */
    private void hideHelpOverlay() {
        if (helpOverlayLayout.getVisibility() == View.VISIBLE) {
            helpOverlayLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Initialize widget(s) from the activity layout and set any required event listeners.
     */
    protected void initWidgets() {
        this.helpOverlayLayout = findViewById(R.id.helpOverlay);
        this.helpOverlayLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                view.setVisibility(View.GONE);
            }
        });
        if (!FacilityApp.tutorial.shown(getString(R.string.pref_facility_ticket))) {
            this.helpOverlayLayout.setVisibility(View.VISIBLE);
        }

        this.layoutLoading = findViewById(R.id.loadingLayout);
        this.layoutLoading.setVisibility(View.GONE);

        this.btnSubmit = findViewById(R.id.submitButton);

        this.btnAddImage = findViewById(R.id.addImageButton);
        this.btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideHelpOverlay();

                if (imageView1.getVisibility() == View.VISIBLE && imageView2.getVisibility() == View.VISIBLE
                        && imageView3.getVisibility() == View.VISIBLE) {
                    Toast.makeText(FacilityTicketForm.this, "You can only add 3 images.", Toast.LENGTH_SHORT).show();
                } else {
                    addImage();
                }
            }
        });

        this.btnScanQRCode = findViewById(R.id.scanQRCodeButton);

        this.imageBtnDescription = findViewById(R.id.descriptionIcon);

        this.imageBtnDeleteImage1 = findViewById(R.id.deleteImage1Button);
        this.imageBtnDeleteImage1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView1.setImageBitmap(null);
                imageView1.setVisibility(View.GONE);
                images.remove(1);
                imageBtnDeleteImage1.setVisibility(View.GONE);
            }
        });

        this.imageBtnDeleteImage2 = findViewById(R.id.deleteImage2Button);
        this.imageBtnDeleteImage2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView2.setImageBitmap(null);
                imageView2.setVisibility(View.GONE);
                images.remove(2);
                imageBtnDeleteImage2.setVisibility(View.GONE);
            }
        });

        this.imageBtnDeleteImage3 = findViewById(R.id.deleteImage3Button);
        this.imageBtnDeleteImage3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView3.setImageBitmap(null);
                imageView3.setVisibility(View.GONE);
                images.remove(3);
                imageBtnDeleteImage3.setVisibility(View.GONE);
            }
        });

        this.imageBtnClose = findViewById(R.id.locationCloseButton);
        this.imageBtnClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                locationLayout.setVisibility(View.GONE);
                location = null;
            }
        });

        this.editTextDescription = findViewById(R.id.descriptionEditText);
        this.editTextDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                hideHelpOverlay();
            }
        });

        this.imageView1 = findViewById(R.id.image1);
        this.imageView2 = findViewById(R.id.image2);
        this.imageView3 = findViewById(R.id.image3);

        this.switchNearMissOpportunity = findViewById(R.id.nearMissOpportunitySwitch);

        this.locationLayout = findViewById(R.id.locationLayout);
        this.locationDescriptionTextView = findViewById(R.id.locationDescriptionTextView);
        this.locationTextView = findViewById(R.id.locationTextView);
    }

}
