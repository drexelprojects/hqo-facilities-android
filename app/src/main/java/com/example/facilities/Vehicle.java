/**
 * Vehicle.java
 * Represents a standard vehicle without any registered license plate.
 */
package com.example.facilities;

public class Vehicle {
    protected String make;
    protected String model;
    protected int year;
    protected boolean isElectric;
    protected String color;

    /**
     * Default constructor for {@code Vehicle}
     */
    public Vehicle() {
        this.make = "";
        this.model = "";
        this.year = -1;
        this.isElectric = false;
        this.color = "";
    }

    /**
     * Constructor for {@code Vehicle}
     * @param make Make of the vehicle (e.g. Honda, Toyota).
     * @param model Model of the vehicle (e.g. Land Cruiser, Wrangler).
     * @param year Year the car was manufactured.
     * @param isElectric Whether or not the vehicle will require electric charging.
     * @param color Color of the vehicle.
     */
    public Vehicle(String make, String model, int year, boolean isElectric, String color) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.isElectric = isElectric;
        this.color = color;
    }

    /**
     * Copy constructor for {@code Vehicle}.
     */
    public Vehicle(Vehicle vehicle) {
        this.make = vehicle.getMake();
        this.model = vehicle.getModel();
        this.year = vehicle.getYear();
        this.isElectric = vehicle.isElectric();
        this.color = vehicle.getColor();
    }

    /**
     * Gets the make of the vehicle.
     * @return Make of the vehicle.
     */
    public String getMake() { return this.make; }

    /**
     * Gets the model of the vehicle.
     * @return Model of the vehicle.
     */
    public String getModel() { return this.model; }

    /**
     * Gets the year of the vehicle.
     * @return Year the vehicle was manufactured.
     */
    public int getYear() { return this.year; }

    /**
     * Gets the electric status of the vehicle.
     * @return True if electric (hybrid or full), false otherwise.
     */
    public boolean isElectric() { return this.isElectric; }

    /**
     * Gets the color of the vehicle.
     * @return Color of the vehicle
     */
    public String getColor() { return this.color; }
}
