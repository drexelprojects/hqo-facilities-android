/**
 * ImageConverter.java
 * Used to convert images between {@code Bitmap} and {@code String} encoded formats. Utilizes
 * compression when converted to {@code Base64} to improve retrieval performance.
 */
package com.example.facilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;

public class ImageConverter {
    private final String TAG = "IMAGE_CONVERTER";

    /**
     * Converts a {@code Bitmap} to a {@code Base64} string. Compresses to quality 50 to
     * increase performance when retrieving from a database.
     * @param bitmap {@code Bitmap} to be compressed and encoded.
     * @return {@code Base64} encoded equivalent of the inputted {@code Bitmap}.
     */
    public String convertBitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        String image = Base64.encodeToString(byteArray, Base64.NO_WRAP);

        try {
            byteArrayOutputStream.close();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        return image;
    }

    /**
     * Converts a {@code Base64} encoded string to a {@code Bitmap}.
     * @param string {@code Base64} encoded string to be converted.
     * @return {@code Bitmap} decoded from the {@code Base64} string.
     */
    public Bitmap convertStringToBitmap(String string) {
        byte [] encodeByte = Base64.decode(string, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);

        return bitmap;
    }
}
