/**
 * TaskDatabaseSubmit.java
 * Performs an {@code AsynTask} and then processes the result through {@code delegate} in {@code onPostExecute(...)}.
 * By processing the result this way, we can achieve a synchronized result from an asynchronous task.
 *
 * Submits data to the database.
 */
package com.example.facilities;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

public class TaskDatabaseSubmit extends AsyncTask<JSONObject, String, JSONObject> implements AsyncResponse {
    private String TAG = "TASK_DATABASE_SUBMIT";

    private Context context;
    private AsyncResponse delegate;
    private String endpoint;

    /**
     * Constructor for {@code TaskDatabaseSubmit}.
     * @param context Conext that the {@code AsyncTask} should operate in.
     * @param delegate Delegate that will process the result once the task is completed.
     * @param endpoint Endpoint to send the volley request to.
     */
    public TaskDatabaseSubmit(Context context, AsyncResponse delegate, String endpoint) {
        this.context = context;
        this.delegate = delegate;
        this.endpoint = endpoint;
    }

    /**
     * Performs the operation in the backgrund (non-UI thread).
     * @param params Data to be submitted to the database.
     * @return Response from the database after submitting the data.
     */
    @Override
    public JSONObject doInBackground(JSONObject...params) {
        VolleyRequestJSON volley = new VolleyRequestJSON(this.endpoint);

        return volley.post(this.context, params[0]);
    }

    /**
     * Send results to delegate upon task finishin.
     * @param result Results to send for processing.
     */
    @Override
    protected void onPostExecute(JSONObject result) {
        this.delegate.processFinish(result);
    }

    /**
     * Handles result from async task(s) and processes it by calling {@code execute} to call
     * its own task.
     * @param result Response from the async task.
     */
    public void processFinish(JSONObject result) {
        this.execute(result);
    }

}
