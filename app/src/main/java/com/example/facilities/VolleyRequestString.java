/**
 * VolleyRequestString.java
 * Implementation of {@code VolleyRequest<T>}. Gets and posts {@code String} requests.
 */
package com.example.facilities;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class VolleyRequestString implements VolleyRequest<String> {
    private String TAG = "VOLLEY_STRING";
    private String url;

    public VolleyRequestString(String url) {
        this.url = url;
    }

    /**
     * Utilizes {@code RequestFuture<>} to make a {@code StringRequest} GET.
     * Queues the request within the context dictated by parameter {@code context}.
     * @param context Context to perform the request in.
     * @return Response of the request.
     */
    @Override
    public String get(Context context) {
        String response = null;
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest request = new StringRequest(Request.Method.GET, this.url, future, future);
        requestQueue.add(request);

        try {
            response = future.get();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        return response;
    }

    /**
     * Utilizes {@code RequestFuture<>} to make a {@code StringRequest} POST.
     * Queues the request within the context dictated by parameter {@code context}.
     * @param context Context to perform the request in.
     * @param obj Payload to be posted in the request.
     * @return Response of the request.
     */
    @Override
    public String post(Context context, String obj) {
        String response = null;
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest request = new StringRequest(Request.Method.POST, url, future, future);
        requestQueue.add(request);

        try  {
            response = future.get();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        return response;
    }

}
