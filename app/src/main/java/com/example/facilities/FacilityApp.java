/**
 * FacilityApp.java
 * Application class for the app.
 */
package com.example.facilities;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

public class FacilityApp extends Application {

    private static final String TAG = "APPLICATION_CLASS";

    // User for contractor staus to be determined
    public static JSONObject user;

    // Tutorial object to scontrol whether or not it gets displayed
    public static Tutorial tutorial;

    @Override
    public void onCreate() {
        super.onCreate();

        user = new JSONObject();
        try {
            user.put("badge_number", "123456789");
            user.put("first_name", "Karen");
            user.put("last_name", "Stuart");
            user.put("email", "karen.stuart@amwater.com");
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }

        Context context = getApplicationContext();
        tutorial = new Tutorial(context,
                getString(R.string.pref_ticket_list),
                getString(R.string.pref_facility_ticket),
                getString(R.string.pref_parking_pass));
    }

}
