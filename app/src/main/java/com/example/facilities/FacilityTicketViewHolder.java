/**
 * FacilityTicketViewHolder.java
 * {@code ViewHolder} for a {@code FacilityTicket} to be displayed on the {@code RecyclerView} of
 * the {@code TicketList}.
 */
package com.example.facilities;

import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.HashMap;

public class FacilityTicketViewHolder extends RecyclerView.ViewHolder {
    private final String TAG = "FACILITY_TICKET_VH";

    private TextView detailDateTextView, detailDescriptionTextView, detailLocationTextView;
    private LinearLayout detailImagesLayout;
    private ImageView detailNearMissOpportunityIcon, detailCompletedTicketIcon, detailExpandButton;

    private final float DEFAULT_HEIGHT = 87.5f;

    /**
     * Constructor for {@code FacilityTicketViewHolder}.
     * @param itemView Layout view to bind values to.
     */
    public FacilityTicketViewHolder(View itemView) {
        super(itemView);

        this.initWidgets();
    }

    /**
     * Binds values to the layout view.
     * @param facilityTicket Ticket containing data to be bound to view.
     */
    public void bind(FacilityTicket facilityTicket) {
        // Set view item values
        this.itemView.setTag(facilityTicket);

        // Creation date
        SimpleDateFormat format = new SimpleDateFormat("dd MMM YYYY '@' hh:mm a");
        String date = format.format(facilityTicket.getCreationDate().getTime());
        this.detailDateTextView.setText(date);

        // Location details
        Location location = facilityTicket.getLocation();
        if (location != null) {
            String locationDetails = location.getDetails();

            String locationString = locationDetails;
            if (locationDetails != null && !locationDetails.isEmpty()) {
                locationString += " / ";
            }
            String locationDescription = location.getDescription();
            locationString += locationDescription;

            if (locationString.trim().isEmpty()) {
                this.detailLocationTextView.setText("");
            } else {
                this.detailLocationTextView.setText(locationString);
            }
        } else {
            this.detailLocationTextView.setText("");
        }

        // Description
        this.detailDescriptionTextView.setText(facilityTicket.getDescription());
        this.checkForLocation();

        // Check if facility ticket is a near-miss opportunity and reveal icon if it returns true
        if (facilityTicket.isNearMissOpportunity()) {
            this.detailNearMissOpportunityIcon.setVisibility(View.VISIBLE);
        } else {
            this.detailNearMissOpportunityIcon.setVisibility(View.GONE);
        }

        // Check if facility ticket is complete and adjust appearance if it returns true
        if (facilityTicket.isCompleted()) {
            this.detailCompletedTicketIcon.setVisibility(View.VISIBLE);
            this.detailExpandButton.setVisibility(View.GONE);
            this.itemView.setBackgroundResource(R.drawable.completed_ticket);
        } else {
            this.detailCompletedTicketIcon.setVisibility(View.GONE);
            this.detailExpandButton.setVisibility(View.VISIBLE);
            this.itemView.setBackgroundResource(R.drawable.ticket);
        }

        // Any images attached to the facility ticket by the user
        HashMap<Integer, Bitmap> images = facilityTicket.getImages();
        LinearLayout imagesLayout = this.detailImagesLayout;
        resetVisibility(imagesLayout);

        for (Bitmap bitmap : images.values()) {

            // Find first available space
            for (int i=0; i<imagesLayout.getChildCount(); i++) {
                ImageView imageView = (ImageView) imagesLayout.getChildAt(i);
                if (imageView.getVisibility() == View.GONE) {
                    imageView.setImageBitmap(bitmap);
                    imageView.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    }

    /**
     * Reset visibility of image views to GONE and imageBitmaps to null for each facility ticket
     * This is done before new bitmap images are assigned to image views
     * @param linearLayout The image layout passed from bind()
     */
    private void resetVisibility(LinearLayout linearLayout) {
        for (int i=0; i < linearLayout.getChildCount(); i++) {
            ImageView imageView = (ImageView) linearLayout.getChildAt(i);
            imageView.setImageBitmap(null);
            imageView.setVisibility(View.GONE);
        }
    }

    /**
     * Checks to see if location TextView is empty before setting the maxLines of the description TextView
     */
    private void checkForLocation() {
        if (this.detailLocationTextView.getText().equals("")) {
            this.detailDescriptionTextView.setMaxLines(2);
        } else {
            this.detailDescriptionTextView.setMaxLines(1);
        }
    }

    /**
     * Initialize widget(s) from layout file and set any required event listeners.
     */
    public void initWidgets() {
        this.detailCompletedTicketIcon = this.itemView.findViewById(R.id.detailCompletedTicketIcon);
        this.detailExpandButton = this.itemView.findViewById(R.id.detailExpandButton);

        this.detailDateTextView = this.itemView.findViewById(R.id.detailDateTextView);
        this.detailDescriptionTextView = this.itemView.findViewById(R.id.detailDescriptionTextView);
        this.detailLocationTextView = this.itemView.findViewById(R.id.detailLocationTextView);
        if (detailLocationTextView.getText().equals("")) {
            detailDescriptionTextView.setMaxLines(2);
        }
        this.detailImagesLayout = this.itemView.findViewById(R.id.detailImagesLayout);
        this.detailNearMissOpportunityIcon = this.itemView.findViewById(R.id.detailNearMissOpportunityIcon);

        this.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutParams params = view.getLayoutParams();

                if (view.isActivated()) {
                    // Deactivate and adjust root view
                    view.setActivated(false);
                    float density = view.getContext().getResources().getDisplayMetrics().density;
                    params.height = (int) (DEFAULT_HEIGHT * density);

                    // Detail-specific adjustments
                    checkForLocation();
                } else {
                    // Activate and adjust root view
                    view.setActivated(true);
                    params.height = LayoutParams.WRAP_CONTENT;

                    // Detail-specific adjustments
                    detailDescriptionTextView.setMaxLines(Integer.MAX_VALUE);
                    detailLocationTextView.setMaxLines(Integer.MAX_VALUE);
                }
            }
        });
    }
}