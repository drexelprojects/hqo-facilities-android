package com.example.facilities;

import android.graphics.Bitmap;

import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import static org.junit.Assert.*;

public class FacilityTicketUnitTest {

    @Test
    public void testFacilityTicket() {
        FacilityTicket facilityTicket = new FacilityTicket("Glass on the floor");
        Location location = new Location("1104", "11", "CCI", "Kitchen");
        HashMap<Integer, Bitmap> images = new HashMap<>();
        facilityTicket.setNearMissOpportunity(true);
        facilityTicket.setImages(images);
        facilityTicket.setCompleted(false);

        assertEquals(facilityTicket.getDescription(), "Glass on the floor");
        assertTrue(facilityTicket.isNearMissOpportunity());
        assertFalse(facilityTicket.isCompleted());
        assertNotNull(facilityTicket.getImages());

        FacilityTicket facilityTicket1 = new FacilityTicket("More glass on the floor", location);
        facilityTicket1.setDescription("Glass in the kitchen");

        assertNotNull(facilityTicket1.getLocation());
        assertEquals(facilityTicket1.getDescription(), "Glass in the kitchen");
    }

    @Test
    public void testLocation() {
        Location location = new Location("1104", "11", "CCI", "Kitchen");

        assertEquals(location.getRoom(), "1104");
        assertEquals(location.getFloor(), "11");
        assertEquals(location.getBuilding(), "CCI");
        assertEquals(location.getDescription(), "Kitchen");
        assertNotNull(location.getDetails());
    }

    @Test
    public void testTicket() {
        Ticket facilityTicket = new FacilityTicket("Glass on the floor");
        Calendar creationDate = Calendar.getInstance();
        facilityTicket.setCreationDate(creationDate);

        Ticket facilityTicket1 = new FacilityTicket("More glass on the floor");
        Calendar creationDate1 = new GregorianCalendar(2018,2,2);
        facilityTicket1.setCreationDate(creationDate1);

        assertEquals(facilityTicket.getType(), "TYPE_FACILITY");
        assertEquals(facilityTicket.getCreationDate(), creationDate);
        //compare to (passed object is in parenthesis)
        // The method returns 0 if the passed argument is equal to this Calendar object.
        //The method returns 1 if the time of this Calendar object is more than the passed object.
        //The method returns -1 if the time of this Calendar object is less than the passed object.
        assertEquals(facilityTicket.compareTo(facilityTicket1), 1);

    }

}
