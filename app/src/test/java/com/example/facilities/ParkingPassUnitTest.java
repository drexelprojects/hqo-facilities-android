package com.example.facilities;

import android.graphics.Bitmap;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.HashMap;

import static org.junit.Assert.*;

public class ParkingPassUnitTest {

    @Mock Vehicle mockVehicle = new Vehicle("Honda", "CR-V", 2005, false, "Grey");

    @Test
    public void testParkingPassTicket() {
        HashMap<Integer, Bitmap> images = new HashMap<>();
        RegisteredVehicle registeredVehicle = new RegisteredVehicle("Honda", "CR-V", 2005, false, "Grey", "ABC1234", "PA");
        ParkingPassTicket parkingPassTicket = new ParkingPassTicket("123456789", "Karen", "Stuart", "karen.stuart@amwater.com", registeredVehicle, false, true, "email", "karen.stuart@amwater.com", false);

        assertEquals(parkingPassTicket.getBadgeNumber(), "123456789");
        assertEquals(parkingPassTicket.getFirstName(), "Karen");
        assertEquals(parkingPassTicket.getLastName(), "Stuart");
        assertEquals(parkingPassTicket.getEmail(), "karen.stuart@amwater.com");

        assertEquals(parkingPassTicket.getLicensePlate(), "ABC1234");
        assertEquals(parkingPassTicket.getState(), "PA");
        assertEquals(parkingPassTicket.getColor(), "Grey");
        assertEquals(parkingPassTicket.getMake(), "Honda");
        assertEquals(parkingPassTicket.getModel(), "CR-V");
        assertEquals(parkingPassTicket.getYear(), 2005);

        assertFalse(parkingPassTicket.hasMultipleVehicles());
        assertFalse(parkingPassTicket.isElectric());
        assertTrue(parkingPassTicket.isHandicapRegistered());
        assertEquals(parkingPassTicket.getPrefContactMethod(), "email");
        assertEquals(parkingPassTicket.getAltContactInfo(), "karen.stuart@amwater.com");
        assertFalse(parkingPassTicket.isContractor());

        parkingPassTicket.setImages(images);
        assertNotNull(parkingPassTicket.getImages());

    }

    @Test(expected = NullPointerException.class)
    public void testRegisteredVehicle() {
        Vehicle vehicle = new Vehicle();
        RegisteredVehicle registeredVehicle = new RegisteredVehicle();
        assertNull(registeredVehicle.getLicensePlate());
    }

    @Test
    public void testMockVehicle() {
        RegisteredVehicle registeredVehicle1 = new RegisteredVehicle(mockVehicle, "ABC1234", "PA");
        assertNotNull(registeredVehicle1.getLicensePlate());
        assertEquals(registeredVehicle1.getLicensePlate().getState(), "PA");
        assertEquals(registeredVehicle1.getLicensePlate().getLicensePlateNumber(), "ABC1234");
    }

    @Test
    public void testVehicle() {
        Vehicle vehicle = new Vehicle();
        Vehicle vehicle1 = new Vehicle(vehicle);

        assertEquals(vehicle1.getMake(), "");
        assertEquals(vehicle1.getModel(), "");
        assertEquals(vehicle1.getYear(), -1);
        assertFalse(vehicle1.isElectric());
        assertEquals(vehicle1.getColor(), "");
    }


}
